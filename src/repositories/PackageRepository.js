import CrudRepository from './CrudRepository'

export default class PackageRepository extends CrudRepository {
  constructor () {
    super('packages')
  }
}