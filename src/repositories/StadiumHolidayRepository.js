import CrudRepository from './CrudRepository'

export default class StadiumHolidayRepository extends CrudRepository {
  constructor () {
    super('holidays')
  }
}