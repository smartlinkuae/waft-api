import CrudRepository from './CrudRepository'

export default class RegionRepository extends CrudRepository {
  constructor () {
    super('regions')
  }
}