import CrudRepository from './CrudRepository'

export default class StadiumRepository extends CrudRepository {
  constructor () {
    super('stadia')
  }
}