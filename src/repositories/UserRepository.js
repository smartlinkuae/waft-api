import CrudRepository from './CrudRepository'

export default class UserRepository extends CrudRepository {
  constructor () {
    super('users')
  }
}