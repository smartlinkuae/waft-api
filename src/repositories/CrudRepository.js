import Repository from "./Repository";
import { serialize } from "object-to-formdata";
import { notifyStore } from "@/stores/NotifyStore";
const notificationStore = new notifyStore();
export default class CrudRepository {
  constructor(resource) {
    this.resource = resource;
  }

  async getList(page, pagination = false, searchFilter = {}) {
    const requestParams = Object.assign({}, searchFilter, {
      page,
    });
    const { data } = await Repository.get(this.resource, {
      params: requestParams,
    });
    return {
      content: pagination ? data.data : data,
      pages: pagination ? data.last_page : null,
      totalItems: pagination ? data.total : null,
      page,
    };
  }

  async get(id) {
    try {
      const { data } = await Repository.get(`${this.resource}/${id}`);
      return data;
    } catch (err) {
      notificationStore.showError(err.message);
    }
  }

  async update(
    item,
    isFormData = false,
    { url = `${this.resource}/${item.id}/update`, type = "POST" }
  ) {
    const id = item.id;
    if (isFormData) item = this.toFormData(item);
    try {
      const data = await Repository({
        method: type,
        url: url,
        data: item,
      });
      if (data.status === 401) notificationStore.showError("Unauthenticated");
      else notificationStore.showSuccess("Updated Successfully");
      return data;
    } catch (err) {
      notificationStore.showError(err.message);
    }
  }

  async delete(id) {
    try {
      const data = await Repository.delete(`${this.resource}/${id}`);
      if (data.status === 401) notificationStore.showError("Unauthenticated");
      else notificationStore.showSuccess("Deleted Successfully");
      return data;
    } catch (err) {
      notificationStore.showError(err.message);
    }
  }

  async create(item, isFormData = false, url = this.resource) {
    if (isFormData) item = this.toFormData(item);
    try {
      const data = await Repository.post(url, item);
      if (data.data) notificationStore.showSuccess("Created Successfully");
      if (data.status === 401) notificationStore.showError("Unauthenticated");
      return data;
    } catch (err) {
      notificationStore.showError(err.message);
    }
  }
  toFormData(data) {
    return serialize(data);
  }
}
