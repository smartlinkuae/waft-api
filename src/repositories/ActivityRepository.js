import CrudRepository from './CrudRepository'

export default class ActivityRepository extends CrudRepository {
  constructor () {
    super('activities')
  }
}