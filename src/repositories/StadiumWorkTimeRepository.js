import CrudRepository from './CrudRepository'

export default class StadiumWorkTimeRepository extends CrudRepository {
  constructor () {
    super('workTimes')
  }
}