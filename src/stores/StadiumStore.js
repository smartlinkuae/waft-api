import { defineStore } from "pinia";
import StadiumRepository from "@/repositories/StadiumRepository";
const stadiumRepository = new StadiumRepository();

export const stadiumStore = defineStore("StadiumStore", {
  actions: {
    // 👉 Fetch all stadiums
    async fetchData(page) {
      return await stadiumRepository.getList(page, true);
    },
    async save(stadium) {
      return stadium.id ? await this.update(stadium) : await this.create(stadium)
    },
    async create(stadium) {
      return await stadiumRepository.create(stadium, true);
    },
    async update(stadium) {
      return await stadiumRepository.update(stadium, true, {url : `stadium/${stadium.id}/update`, type:'POST'});
    },
    async delete(id){
      return await stadiumRepository.delete(id);
    },
    async get(id) {
      return await stadiumRepository.get(id);
    }
  },
});
