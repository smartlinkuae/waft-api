import { defineStore } from "pinia";
import PackageRepository from "@/repositories/PackageRepository";
import StadiumRepository from "@/repositories/StadiumRepository";
const packageRepository = new PackageRepository();
const stadiumRepository = new StadiumRepository();
export const packageStore = defineStore("PackageStore", {
  actions: {
    // 👉 Fetch all packages
    fetchData(page) {
      return packageRepository.getList(page);
    },
    getStadium(id){
      return stadiumRepository.get(id);
    },
    create(Package) {
      return packageRepository.create(Package, true, "packages");
    },
    async update(Package) {
      return await packageRepository.update(Package, false, {url : `packages/${Package.id}`,type : 'PUT'});
    },
    delete(id){
      return packageRepository.delete(id);
    }
  },
});
