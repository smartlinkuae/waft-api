import { defineStore } from "pinia";
import StadiumWorkTimeRepository from "@/repositories/StadiumWorkTimeRepository";
import StadiumRepository from "@/repositories/StadiumRepository";
const stadiumWorkTimeRepository = new StadiumWorkTimeRepository();
const stadiumRepository = new StadiumRepository();
export const stadiumWorkTimeStore = defineStore("stadiumWorkTimeStore", {
  actions: {
    // 👉 Fetch all Stadium Work Times
    fetchData(page) {
      return stadiumWorkTimeRepository.getList(page);
    },
    getStadium(id){
      return stadiumRepository.get(id);
    },
    create(workTime) {
      return stadiumWorkTimeRepository.create(workTime, true);
    },
    update(workTime){
      return stadiumWorkTimeRepository.update(workTime);
    },
    delete(id){
      return stadiumWorkTimeRepository.delete(id);
    }
  },
});
