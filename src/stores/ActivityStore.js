import { defineStore } from "pinia";
import ActivityRepository from "@/repositories/ActivityRepository";
const activityRepository = new ActivityRepository();

export const activityStore = defineStore("ActivityStore", {
  actions: {
    // 👉 Fetch all activities
    async fetchData(page) {
      return await activityRepository.getList(page, false);
    },
    create(activity) {
      return activityRepository.create(activity, true);
    },
    async update(Package) {
      return await activityRepository.update(Package, true, {url : `activity/${Package.id}/update`,type:'POST'});
    },
    delete(id){
      return activityRepository.delete(id);
    }
  },
});
