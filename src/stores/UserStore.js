import { defineStore } from "pinia";
import UserRepository from "@/repositories/UserRepository";
const userRepository = new UserRepository();

export const userStore = defineStore("UserStore", {
  actions: {
    // 👉 Fetch all users
    fetchData(page) {
      return userRepository.getList(page);
    },
    create(user) {
      return userRepository.create(user, true, "register");
    },
  },
});
