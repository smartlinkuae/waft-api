import { defineStore } from "pinia";
import RegionRepository from "@/repositories/RegionRepository";
const regionRepository = new RegionRepository();

export const regionStore = defineStore("RegionStore", {
  actions: {
    // 👉 Fetch all regions
    fetchData(page) {
      return regionRepository.getList(page);
    },
    create(region) {
      return regionRepository.create(region, true);
    },
  },
});
