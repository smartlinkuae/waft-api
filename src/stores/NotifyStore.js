import { defineStore } from "pinia";

export const notifyStore = defineStore("NotifyStore", {
  actions: {
    showSuccess(msg) {
        this.notify = {
            show: true,
            color: 'success',
            timeout: 2000,
            message: msg
        }
    },
    showError(msg) {
        this.notify = {
            show: true,
            color: 'error',
            timeout: 2000,
            message: msg
        }
    }
  },
  state: () => {
    return {
      notify: {
        show: false,
        color: 'primary',
        timeout: 3000,
        message: '',
      }
    }
  },
});
