import { defineStore } from "pinia";
import StadiumHolidayRepository from "@/repositories/StadiumHolidayRepository";
import StadiumRepository from "@/repositories/StadiumRepository";
const stadiumHolidayRepository = new StadiumHolidayRepository();
const stadiumRepository = new StadiumRepository();
export const stadiumHolidayStore = defineStore("stadiumHolidayStore", {
  actions: {
    // 👉 Fetch all Stadium Holidays
    fetchData(page) {
      return stadiumHolidayRepository.getList(page);
    },
    getStadium(id){
      return stadiumRepository.get(id);
    },
    create(holiday) {
      return stadiumHolidayRepository.create(holiday, true);
    },
    update(holiday){
      return stadiumHolidayRepository.update(holiday,false,{url : `holidays/${holiday.id}`,type : 'PUT'});
    },
    delete(id){
      return stadiumHolidayRepository.delete(id);
    }
  },
});
