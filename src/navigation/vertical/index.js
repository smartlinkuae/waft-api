import dashboard from './dashboard'
import stadiums from './stadiums'
import users from './users'
import activities from './activities'

export default [...dashboard, ...stadiums,...activities,...users]
