<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserService extends Model
{
    protected $fillable = ['user_id', 'icon'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
