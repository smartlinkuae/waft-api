<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['booking_id', 'transaction_id','payment_id',  'amount',  'currency',  'status',  'payment_method'];


    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
