<?php

namespace App\Models;

use App\Models\Region;
use App\Models\StadiumService;
use App\Models\WorkTime;
use App\Models\Holiday;
use App\Models\Activity;
use Illuminate\Database\Eloquent\Model;

class Stadium extends Model
{
    protected $fillable = [
        'name_en',
        'name_ar',
        'region_id',
        'activity_id',
        'map_url',
        'description_en',
        'description_ar',
        'address_en',
        'address_ar',
        'phone',
        'email',
        'website',
        'image',
        'slot',
        'price',
        'status'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function users()
    {
        return $this->belongsToMany(User::class, 'user_stadia', 'stadium_id', 'user_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function workTimes()
    {
        return $this->hasMany(workTime::class);
    }

    public function holidays()
    {
        return $this->hasMany(Holiday::class);
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class, 'activity_id');
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites', 'stadium_id', 'user_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
    public function packages()
    {
        return $this->hasMany(Package::class);
    }

    public function stadiumServices()
    {
        return $this->hasMany(StadiumService::class);
    }

    public function services()
    {
        return $this->hasManyThrough(Service::class, StadiumService::class, 'stadium_id', 'id', 'id', 'service_id');
    }

    public function images()
    {
        return $this->hasMany(StadiumImage::class);
    }
}
