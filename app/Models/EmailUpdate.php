<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailUpdate extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'new_email',
        'code',
        'used',
    ];

    protected $casts = [
        'used' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
