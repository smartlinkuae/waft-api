<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StadiumImage extends Model
{
    protected $fillable = ['image_path'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function stadium()
    {
        return $this->belongsTo(Stadium::class);
    }
}
