<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable = ['name_en', 'name_ar',  'city_id'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
