<?php

namespace App\Models;

use App\Models\Booking;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'birthday',
        'gender',
        'password',
        'role_id',
        'image',
        'firebase_token',
        'browsing_mode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the role associated with the user.
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function isAdmin()
    {
        // Check if the user has the "admin" role
        return $this->role_id === Role::where('name', 'admin')->value('id');
    }

    public function isManager()
    {
        // Check if the user has the "Manager" role
        return $this->role_id === Role::where('name', 'manager')->value('id');
    }
    public function isStadiumManager()
    {
        // Check if the user has the "Stadium Manager" role
        return $this->role_id === Role::where('name', 'stadium_manager')->value('id');
    }

    public function ownsStadium($stadiumId)
    {
        return $this->stadia()->where('stadium_id', $stadiumId)->exists();
    }

    public function stadia()
    {
        return $this->belongsToMany(Stadium::class, 'user_stadia', 'user_id', 'stadium_id');
    }
    public function stadiums()
    {
        return $this->belongsToMany(Stadium::class, 'user_stadia', 'user_id', 'stadium_id');
    }

    public function favorites()
    {
        return $this->belongsToMany(Stadium::class, 'favorites', 'user_id', 'stadium_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function favoriteStadiums(): BelongsToMany
    {
        return $this->belongsToMany(Stadium::class, 'favorites');
    }

    public function getPermissions($role_id)
    {
        $permissions = [];

        if ($role_id) {
            $role = Role::find($role_id);
            if ($role) {
                $permissions = $role->permissions()->pluck('name')->all();
            }
        }

        return $permissions;
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
