<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserStadium extends Model
{
    // protected $table = 'user_stadia';

    protected $fillable = ['stadium_id', 'user_id'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
