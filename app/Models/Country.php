<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name_en', 'name_ar', 'iso_code'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
