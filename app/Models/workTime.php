<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkTime extends Model
{
    protected $fillable = ['stadium_id', 'day', 'start_time', 'end_time'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
