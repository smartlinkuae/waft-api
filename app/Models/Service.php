<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name_en', 'name_ar',  'icon'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function stadiumServices()
    {
        return $this->hasMany(StadiumService::class);
    }
}
