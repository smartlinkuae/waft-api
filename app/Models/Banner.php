<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['title_en', 'title_ar', 'image_en', 'image_ar', 'link', 'user_id', 'display_for'];
}
