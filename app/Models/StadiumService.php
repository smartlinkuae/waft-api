<?php

namespace App\Models;

use App\Models\Service;
use App\Models\Stadium;
use Illuminate\Database\Eloquent\Model;

class StadiumService extends Model
{
    protected $fillable = ['stadium_id', 'service_id'];

    public function stadium()
    {
        return $this->belongsTo(Stadium::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
