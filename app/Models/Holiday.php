<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = ['stadium_id', 'name_en', 'name_ar',  'date'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
