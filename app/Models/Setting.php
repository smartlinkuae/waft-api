<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = [
        'app_version_android',
        'app_version_ios',
        'onboarding',
        'support_phone_number',
        'privacy_policy',
        'terms_and_conditions',
        'about_us',

    ];

    protected $casts = [
        'app_version_android' => 'json',
        'app_version_ios' => 'json',
        'onboarding' => 'json',
    ];
}
