<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StadiumActivity extends Model
{
    protected $fillable = ['stadium_id', 'activity_id'];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
