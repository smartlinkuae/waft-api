<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'stadium_id',
        'user_id',
        'name_en',
        'name_ar',
        'price',
        'slot',
        'type',
        'slot_type'
    ];

    // Define the relationship with Stadium model
    public function stadium()
    {
        return $this->belongsTo(Stadium::class);
    }

    // Define the relationship with User model
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
