<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [
        'stadium_id',
        'package_id',
        'user_id',
        'start_time',
        'end_time',
        'type',
        'slot',
        'price',
        'cancelled_at',
        'cancel_reason',
        'cancelled_by'
    ];


    public function stadium()
    {
        return $this->belongsTo(Stadium::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cancelledBy()
    {
        return $this->belongsTo(User::class, 'cancelled_by');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
