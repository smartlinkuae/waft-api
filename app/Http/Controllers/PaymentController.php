<?php

namespace App\Http\Controllers;

use App\Http\Controllers\NotificationController;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Payment;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\ValidationException;

class PaymentController extends Controller
{


    public $outlet = '7e570559-3119-4826-bd48-f2a34adaa854';
    public $apikey = "NGVkYTViMjUtMDY4MS00ZWVhLWFmOTktM2UyMTdhZWMwZWJhOjVlMmJhY2FjLWIyYmMtNGFlNi04ODBiLTA1MjljMWRlZGI4Mw==";		// enter your API key here
    public $currency = "AED";
    public $baseUrl = "https://api-gateway.ngenius-payments.com";

    public function __construct() {
        // if(env('APP_DEBUG')){
        //     $this->outlet = "5f6d9078-f9d1-48a0-8c1e-5e8822352dc6";
        //     $this->apiKey = "ZjM3ZTM2Y2ItMGQ3Zi00ODM1LThlZjktMWQ4MDdkNzcxY2QzOjU3YmNmOGM0LTE3YTgtNGU2NS04NTA4LTk1YjI5NjcwZWMwNA==";
        //     $this->baseUrl = "https://api-gateway.sandbox.ngenius-payments.com";
        // }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    public function getToken(){
       try{
            $ch = curl_init();
            $baseUrl = $this->baseUrl;
            curl_setopt($ch, CURLOPT_URL, $baseUrl."/identity/auth/access-token");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "accept: application/vnd.ni-identity.v1+json",
                "authorization: Basic ".$this->apikey,
                "content-type: application/vnd.ni-identity.v1+json"
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            if(env('APP_DEBUG')){
                // curl_setopt($ch, CURLOPT_POSTFIELDS,  "{\"realmName\":\"ni\"}");
            }
            $output = json_decode(curl_exec($ch));
            return $output->access_token;
       }catch(\Exception $e){
            return '';
       }
    }

    public function getStatus($ref){
        try{
            $token = $this->getToken();
            $ch = curl_init();
            $outlet = $this->outlet;
            $baseUrl = $this->baseUrl;

            curl_setopt_array($ch, [
                CURLOPT_URL => $baseUrl."/transactions/outlets/$outlet/orders/$ref",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                "Authorization: Bearer $token",
                "accept: application/vnd.ni-payment.v2+json"
                ],
            ]);
            $output = json_decode(curl_exec($ch));
            return $output;

        }catch(\Exception $e){
            return dd($e);
        }
    }



    public function createPayment(Request $request)
    {
        $this->getStatus($request->id);

      try{

        $request->validate([
            "booking_id" => "required|exists:bookings,id",
        ]);

        $bookingId = $request->input("booking_id");
        $booking = Booking::findOrFail($bookingId); // Assuming you have a Booking model
        $startTime = strtotime($booking->start_time);
        $endTime = strtotime($booking->end_time);
        $durationMinutes = ceil(($endTime - $startTime) / 60);

        // Prepare the payment parameters
        $amount = $booking->price * $durationMinutes / $booking->slot;
        $description = "Stadium booking payment";
        $redirectUrl = "https://waft.ae/bookingResult?bookingId=$bookingId";

        $user = Auth::user();
        /////////////////////////////////////////
        $postData = new \StdClass();
        $postData->action = "SALE";
        $postData->amount = new \StdClass();
        $postData->amount->currencyCode = $this->currency;
        $postData->amount->value = $amount * 100;
        $postData->merchantAttributes = new \StdClass();
        $postData->merchantAttributes->redirectUrl = $redirectUrl;
        $json = json_encode($postData);

        // get Access Token
        $token = $this->getToken();
        $baseUrl = $this->baseUrl;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $baseUrl."/transactions/outlets/".$this->outlet."/orders");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: Bearer ".$token,
        "Content-Type: application/vnd.ni-payment.v2+json",
        "Accept: application/vnd.ni-payment.v2+json"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $output = json_decode(curl_exec($ch));
        ////////////////////////////////////////////////////////////////////////

        $order_reference = $output->reference;
        $order_paypage_url = $output->_links->payment->href;
        $paymentId = $output->_embedded->payment[0]->reference;
        $payment = new Payment([
            "booking_id" => $bookingId,
            "transaction_id" => $output->reference,
            "payment_id" => $paymentId,
            "status" => "PENDING",
            "amount" => $amount,
            "currency" => $this->currency,
            "payment_method" => "card",
        ]);
        $payment->save();
        return response()->json(["message" => "Create Payment successfully",   "url" => $order_paypage_url]);
        curl_close ($ch);
      }catch(Exception $e){
        return response()->json(["error" => $e], 500);
        }
    }

    public function handleCallback(Request $request)
    {

        try {
            $request->validate([
                "id" => "required"
            ]);

            $transactionId = $request->id;

            $payment = Payment::where("transaction_id", $transactionId)->first();

            if (!$payment) {
                // Handle payment not found error
                // Log the error and take appropriate action
                return response()->json(["error" => "Payment not found."], 404);
            }

            $output = $this->getStatus($transactionId);
            $state = $output->_embedded->payment[0]->state;

            $payment->status = $state;
            $payment->save();
            /// Perform any necessary actions based on the payment result
            if ($payment->status === "CAPTURED") {
                // Payment successful

                // Update the booking status or perform any other necessary actions
                $booking = Booking::find($payment->booking_id);
                $booking->completed_at = now(); // Set the completed_at status to the current time
                $booking->save();

                // Send notification for successful payment
                $notificationController = new NotificationController();

                $notificationData = [
                    "user_ids" => [$booking->user_id],
                    "title" => "Payment Successful",
                    "body" => "Your payment for booking ID: $booking->id was successful.",
                ];
                $notificationController->sendNotificationWithStore(new Request($notificationData));
            } else {
                // Payment failed or not captured
                // Handle the failure scenario
            }

            // Return a response to Tap to acknowledge the payment callback
            return response()->json(["message" => "Ok",   "data" => $payment->status]);
        } catch (ValidationException $e) {
            return response()->json(["errors" => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(["error" => "Payment not found."], 404);
        } catch (\Exception $e) {
            return response(["error" => "An error occurred during Process Payment.", "message" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    public function canclePayment($output, $link){
        try{
            $ch = curl_init();
            $token =$this->getToken();
            curl_setopt_array($ch, [
                CURLOPT_URL => $link,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "DELETE",
                CURLOPT_HTTPHEADER => [
                "Authorization: Bearer $token",
                "accept: application/vnd.ni-payment.v2+json"
                ],
            ]);
            $output = json_decode(curl_exec($ch));
            return $output;
        }catch(\Exception $e){
            return null;
        }

    }

    public function refundPayment($output, $link, $amount){
        try{
            $ch = curl_init();
            $token =$this->getToken();
            $amount = new \StdClass();
            $amount->currencyCode = $this->currency;
            $amount->value = $value * 100;
            $json = json_encode($amount);
            curl_setopt_array($ch, [
                CURLOPT_URL => $link,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => [
                "Authorization: Bearer $token",
                "accept: application/vnd.ni-payment.v2+json",
                "content-type: application/vnd.ni-payment.v2+json"
                ],
            ]);
            $output = json_decode(curl_exec($ch));
            return $output;
        }catch(\Exception $e){
            return null;
        }
    }

    public function refundHandle(Request $request)
    {
        try {
            $request->validate([
                "booking_id" => "required|exists:bookings,id",
                "reason" => "required|string"
            ]);

            $reason = $request->input("reason");
            $bookingId = $request->input("booking_id");
            $booking = Booking::with("payments")->findOrFail($bookingId);
            // Check if the booking is cancelled
            if (!$booking->cancelled_at) {
                return response()->json(["error" => "Booking is not cancelled. Cannot refund."], 400);
            }

            // Check if the booking is associated with a payment
            if (!$booking->payments()->exists()) {
                return response()->json(["error" => "Payment not found for the booking."], 404);
            }

            $payment = $booking->payments->last();
            // Check if the payment status is CAPTURED


            $token = $this->getToken();

            $output = $this->getStatus($payment->transaction_id);

            $state = $output->_embedded->payment[0]->state;
            if ($state !== "CAPTURED" && $state !== 'PURCHASED') {
                return response()->json(["error" => "Payment is not captured. Cannot refund."], 400);
            }
            $links = $output->_embedded->payment[0]->_embedded->{'cnp:capture'}[0]->_links;
            if(isset($links->{'cnp:refund'})){
                $link = $links->{'cnp:refund'}->href;
                $res = $this->refundPayment($output, $link,(int)($payment->amount));
            }else{
                $link = $links->self->href;
                $res = $this->canclePayment($output, $link,);
            }

            // $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // Send notification for successful refund
            $notificationController = new NotificationController();

            // Process the refund result
            if ($res != null) {
                // Refund successful
                // Create a new payment record for the refund
                $refundPayment = new Payment();
                $refundPayment->booking_id = $bookingId;
                $refundPayment->amount = -$payment->amount; // Negative amount for the refund
                $refundPayment->currency = $this->currency;
                $refundPayment->status = "REFUNDED";
                $refundPayment->transaction_id = $payment->transaction_id; // Save the original transaction ID to keep track of the refund
                $refundPayment->purchase_id = $payment->purchase_id; // Save the original transaction ID to keep track of the refund
                $refundPayment->payment_id = $payment->payment_id;
                $refundPayment->payment_method = "card";

                $refundPayment->save();

                $notificationData = [
                    "user_ids" => [$booking->user_id],
                    "title" => "Refund Successful",
                    "body" => "Your refund for booking ID: $bookingId was successful.",
                ];
                $notificationController->sendNotificationWithStore(new Request($notificationData));

                // Perform any other necessary actions
                // For example, update the booking status or handle other operations related to the refund

                return response()->json(["message" => "Refund successful", "data" => $res],200);
            } else {
                // Refund failed
                // Log the error and take appropriate action

                $errorData = $res;

                $notificationData = [
                    "user_ids" => [$booking->user_id],
                    "title" => "Refund Failed",
                    "body" => "Your refund for booking ID: $bookingId was failed.",
                ];
                $notificationController->sendNotificationWithStore(new Request($notificationData));

                return response()->json(["message" => "Refund failed", "errorData" => $errorData],500);
            }
        } catch (ValidationException $e) {
            return response()->json(["errors" => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            return response()->json(["error" => "Booking not found."], 404);
        } catch (\Exception $e) {
            return response(["error" => "An error occurred during the refund process.", "message" => $e->getMessage()], 500);
        }
    }


    // Get payment history for the authenticated user
    public function getPaymentHistory(Request $request)
    {
        $userId = Auth::id();
        $perPage = $request->query("per_page", 10);
        $page = $request->query("page", 1);

        $paymentHistory = Payment::whereHas("booking", function ($query) use ($userId) {
            $query->where("user_id", $userId);
        })
            ->orderBy("created_at", "desc")
            ->paginate($perPage, ["*"], "page", $page);

        return response()->json($paymentHistory);
    }



    public function createAuthrzedPayment($orderRef, $paymentRef, $value){
        try{
            $outlet = $this->outlet;
            $token = $this->getToken();
            $baseUrl = $this->baseUrl;

            $url = $baseUrl."/transactions/outlets/$outlet/orders/$orderRef/payments/$paymentRef/captures";
            $curl = curl_init();
            $amount = new \StdClass();
            $amount->currencyCode = $this->currency;
            $amount->value = $value * 100;
            $json = json_encode($amount);
            curl_setopt_array($curl, [
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $json,
              CURLOPT_HTTPHEADER => [
                "Authorization: Bearer ".$token,
                "accept: application/vnd.ni-payment.v2+json",
                "content-type: application/vnd.ni-payment.v2+json"
              ],
            ]);


            $output = json_decode(curl_exec($curl));
            curl_close($curl);

            return $output;
        }catch(\Exception $e){
            dd($e);
        }

    }
}
