<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;


class NotificationController extends Controller
{



    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    public function sendNotification_by_device_token(Request $request)
    {
        $tokens = $request->tokens;
        $title = $request->title;
        $body = $request->body;
        $this->sendFirebaseNotificationFunc($tokens, $title, $body);

        return response()->json(['message' => 'Notification sent successfully']);
    }

    public function sendNotification(Request $request, $userId)
    {
        $user = User::findOrFail($userId);
        $title = $request->title;
        $body = $request->body;
        $this->sendFirebaseNotificationFunc([$user->firebase_token], $title, $body);

        return response()->json(['message' => 'Notification sent successfully']);
    }

    public function sendFirebaseNotificationFunc($tokens, $title, $body)
    {
        $SERVER_API_KEY = env('FIREBASE_SERVER_KEY');
        Http::withOptions(['verify' => false])
            ->withHeaders([
                'Authorization' => "key=$SERVER_API_KEY"
            ])->post('https://fcm.googleapis.com/fcm/send', [
                'registration_ids' => $tokens,
                'notification' =>   [
                    "body" => $body,
                    "title" => $title,
                    "content_available" => true,
                    "priority" => "hight"
                ],
            ]);
    }

    public function sendNotificationWithStore(Request $request)
    {
        $request->validate([
            'title' => 'nullable',
            'body' => 'required',
            "user_ids" => "required|array",
        ]);

        $userIds = $request->user_ids;
        $title = $request->title;
        $body = $request->body;

        // Store the notification for each user
        foreach ($userIds as $userId) {
            $user = User::findOrFail($userId);
            $firebaseToken = $user->firebase_token;

            // Send the notification
            $this->sendFirebaseNotificationFunc([$firebaseToken], $title, $body);

            // Store the notification for the user
            $notification = new Notification();
            $notification->user_id = $userId;
            $notification->title = $title;
            $notification->body = $body;
            $notification->save();
        }

        return response()->json(["message" => "Notifications sent and stored successfully"]);
    }


    public function markAsRead($id)
    {
        $notification = Notification::findOrFail($id);
        $notification->is_read = true;
        $notification->save();

        return response()->json(['message' => 'Notification marked as read']);
    }

    public function getUnreadNotifications(Request $request)
    {
        $user = Auth::user();
        $userId = $user->id;

        $notifications = Notification::where('user_id', $userId)
            ->where('is_read', false)
            ->get();

        return response()->json($notifications);
    }

    public function getNotifications(Request $request)
    {
        $user = Auth::user();
        $userId = $user->id;

        $perPage = $request->query('per_page', 10); // Number of records per page, default is 10
        $page = $request->query('page', 1); // Current page number, default is 1

        $notifications = Notification::where('user_id', $userId)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);

        $notifications->setCollection($notifications->getCollection());

        return response()->json($notifications);
    }
}
