<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class BannerController extends Controller
{
    public function __construct()
    {
        // $this->middleware('admin')->only(['store', 'update', 'destroy']);
    }

    public function index(Request $request)
    {
        try {
            $request->validate([
                'display_for' => 'nullable|in:stadium_manager,user',
            ]);

            $query = Banner::query();

            // Filter by the 'display_for' field if provided
            $displayForFilter = $request->query('display_for');
            if ($displayForFilter) {
                $query->where('display_for', $displayForFilter);
            }

            $banners = $query->get();
            return response()->json($banners);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to fetch banners', 'message' => $e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $banner = Banner::findOrFail($id);
        return response()->json($banner);
    }

    public function store(Request $request)
    {
        try {
            $isAdmin = auth('api')->user()->isAdmin(); // Check if the user is an admin

            $validatedData = $request->validate([
                'title_en' => 'required',
                'title_ar' => 'required',
                'image_en' => 'required|image',
                'image_ar' => 'required|image',
                'link' => 'required|url',
                'display_for' => 'nullable|in:stadium_manager,user',
                'user_id' => 'nullable|exists:users,id'
            ]);

            // Store the English image
            $imageEn = $request->file('image_en');
            $imageEnExtension = $imageEn->getClientOriginalExtension();
            $imageEnName = Str::uuid()->toString() . '.' . $imageEnExtension;
            $imageEnPath = $imageEn->storeAs('images/banners', $imageEnName, 'public');
            $imageEnURL = url('storage/' . $imageEnPath);

            // Store the Arabic image
            $imageAr = $request->file('image_ar');
            $imageArExtension = $imageAr->getClientOriginalExtension();
            $imageArName = Str::uuid()->toString() . '.' . $imageArExtension;
            $imageArPath = $imageAr->storeAs('images/banners', $imageArName, 'public');
            $imageArURL = url('storage/' . $imageArPath);

            // Determine the user_id based on the user
            $user_id = $request->input('user_id', null);
            $userId = $isAdmin ? $user_id : auth('api')->user()->id;

            $banner = Banner::create([
                'title_en' => $validatedData['title_en'],
                'title_ar' => $validatedData['title_ar'],
                'image_en' => $imageEnURL,
                'image_ar' => $imageArURL,
                'link' => $validatedData['link'],
                'display_for' => $validatedData['display_for'] ?? 'user',
                'user_id' => $userId,
            ]);

            return response()->json($banner, 201);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to add banner', 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $isAdmin = auth('api')->user()->isAdmin(); // Check if the user is an admin

            $validatedData = $request->validate([
                'title_en' => 'required',
                'title_ar' => 'required',
                'image_en' => 'sometimes|image',
                'image_ar' => 'sometimes|image',
                'link' => 'required|url',
                'display_for' => 'nullable|in:stadium_manager,user',
            ]);
            $banner = Banner::findOrFail($id);

            $bannerData = $validatedData;

            // If the user is not an admin, check if they own the banner
            if (!$isAdmin && $banner->user_id !== auth('api')->user()->id) {
                return response()->json(['error' => 'You are not authorized to update this banner.'], Response::HTTP_FORBIDDEN);
            }

            if ($request->hasFile('image_en')) {
                $oldImageEnPath = $banner->image_en;

                // Store the new English image
                $imageEn = $request->file('image_en');
                $imageEnExtension = $imageEn->getClientOriginalExtension();
                $imageEnName = Str::uuid()->toString() . '.' . $imageEnExtension;
                $imageEnPath = $imageEn->storeAs('images/banners', $imageEnName, 'public');
                $imageEnURL = url('storage/' . $imageEnPath);
                $bannerData['image_en'] = $imageEnURL;

                // Delete the old English image if it exists
                if (!empty($oldImageEnPath) && Storage::disk('public')->exists($oldImageEnPath)) {
                    Storage::disk('public')->delete($oldImageEnPath);
                }
            }

            if ($request->hasFile('image_ar')) {
                $oldImageArPath = $banner->image_ar;

                // Store the new Arabic image
                $imageAr = $request->file('image_ar');
                $imageArExtension = $imageAr->getClientOriginalExtension();
                $imageArName = Str::uuid()->toString() . '.' . $imageArExtension;
                $imageArPath = $imageAr->storeAs('images/banners', $imageArName, 'public');
                $imageArURL = url('storage/' . $imageArPath);
                $bannerData['image_ar'] = $imageArURL;
                // Delete the old Arabic image if it exists
                if (!empty($oldImageArPath) && Storage::disk('public')->exists($oldImageArPath)) {
                    Storage::disk('public')->delete($oldImageArPath);
                }
            }

            $bannerData['display_for'] = $bannerData['display_for'] ?? 'user';
            $banner->update($bannerData);

            return response()->json($banner);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Banner not found'], Response::HTTP_NOT_FOUND);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update Banner', 'message' => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $isAdmin = auth('api')->user()->isAdmin(); // Check if the user is an admin
            $banner = Banner::findOrFail($id);

            // If the user is not an admin, check if they own the banner
            if (!$isAdmin && $banner->user_id !== auth('api')->user()->id) {
                return response()->json(['error' => 'You are not authorized to delete this banner.'], Response::HTTP_FORBIDDEN);
            }

            $banner->delete();

            return response(['message' => 'Banner deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Banner not found.'], 404);
        }
    }
}
