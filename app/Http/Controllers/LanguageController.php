<?php

namespace App\Http\Controllers;

use App\Models\Language;

class LanguageController extends Controller
{

    public function index()
    {
        $languages = Language::all();

        // Return the languages as a JSON response
        return response()->json($languages);
    }
}
