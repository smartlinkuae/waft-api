<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class RegionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('index, show');
        $this->middleware('admin')->only(['store', 'update', 'destroy']);
    }

    public function index()
    {
        $regions = Region::all();
        return response()->json($regions);
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required|unique:regions|max:255',
                'name_ar' => 'required|unique:regions|max:255',
                'city_id' => 'required|exists:cities,id',
            ]);

            $region = Region::create($validatedData);

            return response()->json($region, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function show($id)
    {
        $region = Region::findOrFail($id);
        return response()->json($region);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => [
                    'required',
                    'max:255',
                    Rule::unique('regions')->ignore($id),
                ],
                'name_ar' => [
                    'required',
                    'max:255',
                    Rule::unique('regions')->ignore($id),
                ],
                'city_id' => 'required|exists:cities,id',
            ]);

            $region = Region::findOrFail($id);
            $region->update($validatedData);

            return response()->json($region);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function destroy($id)
    {
        $region = Region::findOrFail($id);
        $region->delete();

        return response()->json(null, 204);
    }
}
