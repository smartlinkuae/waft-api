<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
    }


    public function index()
    {
        $services = Service::all();
        return response()->json($services);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'icon' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $serviceData = $request->except('icon');

        if ($request->hasFile('icon')) {
            // Upload the icon image
            $iconFile = $request->file('icon');
            $iconPath = $iconFile->store('images/icons', 'public');
            $serviceData['icon'] = url('storage/' . $iconPath);
        }

        $service = Service::create($serviceData);
        return response()->json($service, 201);
    }

    public function show($id)
    {
        $service = Service::findOrFail($id);
        return response()->json($service);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'icon' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        $serviceData = $request->except('icon');

        $service = Service::findOrFail($id);

        if ($request->hasFile('icon')) {
            // Delete the old icon image if it exists
            if (!empty($service->icon) && Storage::disk('public')->exists(str_replace(url('storage/'), '', $service->icon))) {
                Storage::disk('public')->delete(str_replace(url('storage/'), '', $service->icon));
            }

            // Upload the new icon image
            $iconFile = $request->file('icon');
            $iconPath = $iconFile->store('images/icons', 'public');
            $serviceData['icon'] = url('storage/' . $iconPath);
        }

        $service->update($serviceData);
        return response()->json($service);
    }

    public function destroy($id)
    {
        $service = Service::findOrFail($id);

        // Delete the icon image if it exists
        if (!empty($service->icon) && Storage::disk('public')->exists(str_replace(url('storage/'), '', $service->icon))) {
            Storage::disk('public')->delete(str_replace(url('storage/'), '', $service->icon));
        }

        $service->delete();
        return response()->json(null, 204);
    }
}
