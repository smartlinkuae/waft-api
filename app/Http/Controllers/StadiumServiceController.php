<?php

namespace App\Http\Controllers;

use App\Models\StadiumService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class StadiumServiceController extends Controller
{
    public function index()
    {
        try {
            $stadiumServices = StadiumService::all();
            return response()->json($stadiumServices);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to retrieve stadium services.'], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'service_id' => 'required|exists:services,id',
                'description' => 'nullable',
                'price' => 'required|numeric|min:0',
            ]);

            $stadiumService = StadiumService::create($validatedData);

            return response()->json($stadiumService, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to create stadium service.'], 500);
        }
    }

    public function show($id)
    {
        try {
            $stadiumService = StadiumService::findOrFail($id);
            return response()->json($stadiumService);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Stadium service not found.'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'service_id' => 'required|exists:services,id',
                'description' => 'nullable',
                'price' => 'required|numeric|min:0',
            ]);

            $stadiumService = StadiumService::findOrFail($id);
            $stadiumService->update($validatedData);

            return response()->json($stadiumService);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update stadium service.'], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $stadiumService = StadiumService::findOrFail($id);
            $stadiumService->delete();

            return response()->json(['message' => 'Stadium service deleted successfully']);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'stadium service not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to delete stadium service.'], 500);
        }
    }
}
