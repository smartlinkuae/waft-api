<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index');
        $this->middleware('admin')->only(['store', 'update', 'destroy']);
    }

    public function index()
    {
        $countries = Country::all();
        return response()->json($countries);
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required|unique:countries|max:255',
                'name_ar' => 'required|unique:countries|max:255',
                'iso_code' => 'required|unique:countries|max:255',
            ]);

            $country = Country::create($validatedData);

            return response()->json($country, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function show($id)
    {
        $country = Country::with('cities.regions')->findOrFail($id);
        return response()->json($country);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => [
                    'required',
                    'max:255',
                    Rule::unique('countries')->ignore($id),
                ],
                'name_ar' => [
                    'required',
                    'max:255',
                    Rule::unique('countries')->ignore($id),
                ],
                'iso_code' => [
                    'required',
                    'max:255',
                    Rule::unique('countries')->ignore($id),
                ],
            ]);

            $country = Country::findOrFail($id);
            $country->update($validatedData);

            return response()->json($country);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $country->delete();

        return response()->json(null, 204);
    }
}
