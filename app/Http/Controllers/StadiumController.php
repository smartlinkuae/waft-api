<?php

namespace App\Http\Controllers;

use App\Models\Stadium;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class StadiumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show', 'searchStadia');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $request->validate([
                'per_page' => 'nullable|int',
                'page' => 'nullable|int',
                // 'sort_by' => 'in:id,name_en,name_ar,activity_id,address,phone,email,website',
                'sort_order' => 'in:asc,desc',
                'search_value' => 'nullable|string',
                'search_columns' => 'nullable|array', // New parameter for search columns
            ]);

            // Retrieve query parameters
            $language = $request->header('Accept-Language');
            $perPage = $request->query('per_page', 10); // Number of records per page, default is 10
            $page = $request->query('page', 1); // Current page number, default is 1
            $sortBy = $request->query('sort_by', 'id'); // Default sorting column is 'id'
            $sortOrder = $request->query('sort_order', 'asc'); // Default sorting order is 'asc'
            $searchValue = $request->query('search_value');
            $searchColumns = $request->query('search_columns');

            // Start building the query
            $query = Stadium::with('reviews');

            $user = auth('api')->user();

            // Apply "Approved" condition
            if ($user && !$user->isAdmin()) {
                // Add the condition to retrieve only approved stadiums
                $query->where('status', 'Approved');
            }

            // Apply search condition if search value and columns are provided
            if ($searchValue && $searchColumns) {
                $query->where(function ($query) use ($searchValue, $searchColumns) {
                    foreach ($searchColumns as $column) {
                        $query->orWhere($column, 'LIKE', '%' . $searchValue . '%');
                    }
                });
            }

            // Continue with sorting and pagination
            $stadia = $query
                ->orderBy($sortBy, $sortOrder)
                ->paginate($perPage, ['*'], 'page', $page);

            $stadiumsData = $stadia->getCollection()->map(function ($stadium) use ($user, $language) {
                $rating = $stadium->reviews->avg('rating');
                $reviewsCount = $stadium->reviews->count();

                return [
                    'id' => $stadium->id,
                    'name' => $language == 'ar' ? $stadium->name_ar : $stadium->name_en,
                    'name_en' => $stadium->name_en,
                    'name_ar' => $stadium->name_ar,
                    'region_id' => $stadium->region_id,
                    'activity_id' => $stadium->activity_id,
                    'map_url' => $stadium->map_url,
                    'description' =>  $language == 'ar' ? $stadium->description_ar : $stadium->description_en,
                    'address' =>  $language == 'ar' ? $stadium->address_ar : $stadium->address_en,
                    'phone' => $stadium->phone,
                    'email' => $stadium->email,
                    'website' => $stadium->website,
                    'slot' => $stadium->slot,
                    'price' => $stadium->price,
                    'image' => $stadium->image,
                    'status' => $stadium->status,
                    'rating' => $rating,
                    'reviews_count' => $reviewsCount,
                    'is_favorite' => $user ? $user->favorites->contains($stadium->id) : null,
                    'favorite_id' => $user ?  $user->favorites()->select('favorites.id')->where('stadium_id', $stadium->id)->value('id') : null,
                ];
            });

            $stadia->setCollection($stadiumsData);

            return response()->json($stadia);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed To Fetch All', 'message' =>  $e->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required',
                'name_ar' => 'required',
                'region_id' => 'required|exists:regions,id',
                'activity_id' => 'required|exists:activities,id',
                'map_url' => 'nullable|url',
                'description_en' => 'nullable',
                'description_ar' => 'nullable',
                'address_en' => 'nullable',
                'address_ar' => 'nullable',
                'phone' => 'nullable',
                'email' => 'nullable|email',
                'website' => 'nullable|url',
                'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', // Add image validation rules
                'images.*' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', // Add image validation rules

            ]);

            $stadiumData = $validatedData;

            // Handle image upload
            if ($request->hasFile('image')) {
                // Generate a unique filename based on the current date and a random number
                $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $imagePath = $request->file('image')->storeAs('images/stadia', $imageName, 'public');
                $imageURL = url('storage/' . $imagePath);

                $stadiumData['image'] = $imageURL;
            }

            // Create the stadium first without images
            $stadium = Stadium::create($stadiumData);

            // Check if the authenticated user is a stadium owner
            $user = Auth::user();

            if ($user->isAdmin() || $user->isManager()) {
                // If the authenticated user is an admin, set the status to 'Approved'
                $stadiumData['status'] = 'Approved';
            }

            if ($user->isStadiumManager()) {
                // Associate the stadium with the user in the user_stadia pivot table
                $user->stadia()->attach($stadium->id);
            }

            // Handle image upload for stadium images
            if ($request->hasFile('images')) {
                $images = [];

                foreach ($request->file('images') as $image) {
                    $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                    $imagePath = $image->storeAs('images/stadium_images', $imageName, 'public');
                    $imageURL = url('storage/' . $imagePath);

                    $images[] = ['image_path' => $imageURL];
                }

                // Associate the images with the stadium
                $stadium->images()->createMany($images);
            }

            // Eager load the images relationship
            $stadium->load('images');

            return response()->json($stadium, 201);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to Save stadium.', 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stadium  $stadium
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $language = $request->header('Accept-Language');

        $user = auth('api')->user();

        $stadium = Stadium::with(['region', 'activity', 'workTimes', 'holidays', 'packages', 'stadiumServices.service'])->find($id);

        if (!$stadium) {
            return response()->json(['message' => 'Stadium not found'], 404);
        }

        // Calculate the average rating and count of reviews
        $ratingData = DB::table('reviews')
            ->select(DB::raw('AVG(rating) AS average_rating'), DB::raw('COUNT(*) AS review_count'))
            ->where('stadium_id', $stadium->id)
            ->first();

        $averageRating = $ratingData->average_rating;
        $reviewCount = $ratingData->review_count;

        $isFavorite = $user ?  $user->favorites()->where('favorites.stadium_id', $stadium->id)->exists() : null;
        $favoriteId = $user ? ($isFavorite ? $user->favorites()->where('favorites.stadium_id', $stadium->id)->value('favorites.id') : null) : null;

        $stadiumData = [
            'id' => $stadium->id,
            'name' => $language == 'ar' ? $stadium->name_ar : $stadium->name_en,
            'name_en' => $stadium->name_en,
            'name_ar' => $stadium->name_ar,
            'region_id' => $stadium->region_id,
            'region_name' => $stadium->region ? ($language == 'ar' ? $stadium->region->name_ar : $stadium->region->name_en) : null,
            'activity_id' => $stadium->activity_id,
            'activity_name' => $stadium->activity ? ($language == 'ar' ? $stadium->activity->name_ar : $stadium->activity->name_en) : null,
            'map_url' => $stadium->map_url,
            'description' =>  $language == 'ar' ? $stadium->description_ar : $stadium->description_en,
            'description_en' => $stadium->description_en,
            'description_ar' => $stadium->description_ar,
            'address' =>  $language == 'ar' ? $stadium->address_ar : $stadium->address_en,
            'address_en' => $stadium->address_en,
            'address_ar' => $stadium->address_ar,
            'phone' => $stadium->phone,
            'email' => $stadium->email,
            'website' => $stadium->website,
            'image' => $stadium->image,
            'status' => $stadium->status,
            'price' => $stadium->price,
            'slot' => $stadium->slot,
            'images' => $stadium->images,
            'workTimes' => $stadium->workTimes,
            'holidays' => $stadium->holidays,
            'packages' => $stadium->packages,
            'services' => $stadium->stadiumServices->map(function ($stadiumServices, $language) {
                // Add the 'stadium_service_id' to each service data
                return [
                    'stadium_service_id' => $stadiumServices->id,
                    'price' => $stadiumServices->price,
                    'service_id' => $stadiumServices->service_id,
                    'id' => $stadiumServices->service->id,
                    'name' => $language == 'ar' ? $stadiumServices->service->name_ar : $stadiumServices->service->name_en,
                    'name_en' => $stadiumServices->service->name_en,
                    'name_ar' => $stadiumServices->service->name_ar,
                    'icon' => $stadiumServices->service->icon,
                ];
            }),
            'average_rating' => $averageRating,
            'review_count' => $reviewCount,
            'is_favorite' => $isFavorite,
            'favorite_id' => $favoriteId,
        ];

        return response()->json($stadiumData);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required',
                'name_ar' => 'required',
                'region_id' => 'required|exists:regions,id',
                'activity_id' => 'required|exists:activities,id',
                'map_url' => 'nullable|url',
                'description_en' => 'nullable',
                'description_ar' => 'nullable',
                'address_en' => 'nullable',
                'address_ar' => 'nullable',
                'phone' => 'nullable',
                'email' => 'nullable|email',
                'website' => 'nullable|url',
                'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', // Add image validation rules
                'images.*' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', // Add image validation rules
            ]);

            $stadium = Stadium::findOrFail($id);
            $stadiumData = $validatedData;

            // Handle image upload
            if ($request->hasFile('image')) {
                $oldImagePath = $stadium->image;


                // Generate a unique filename based on the current date and a random number
                $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $imagePath = $request->file('image')->storeAs('images/stadia', $imageName, 'public');
                $imageURL = url('storage/' . $imagePath);

                $stadiumData['image'] = $imageURL;

                // Delete the old image if it exists
                if (!empty($oldImagePath) && Storage::disk('public')->exists($oldImagePath)) {
                    Storage::disk('public')->delete($oldImagePath);
                }
            }

            // Update the stadium data
            $stadium->update($stadiumData);

            // Handle image upload for stadium images
            if ($request->hasFile('images')) {
                $images = [];

                foreach ($request->file('images') as $image) {
                    $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                    $imagePath = $image->storeAs('images/stadium_images', $imageName, 'public');
                    $imageURL = url('storage/' . $imagePath);

                    $images[] = ['image_path' => $imageURL];
                }

                // Delete the old images if they exist
                if ($stadium->images()->exists()) {
                    $oldImages = $stadium->images()->get();

                    foreach ($oldImages as $oldImage) {
                        if (Storage::disk('public')->exists('images/stadium_images/' . basename($oldImage->url))) {
                            Storage::disk('public')->delete('images/stadium_images/' . basename($oldImage->url));
                        }
                    }

                    // Delete the old images from the database
                    $stadium->images()->delete();
                }

                // Associate the new images with the stadium
                $stadium->images()->createMany($images);
            }

            // Eager load the images relationship
            $stadium->load('images');

            return response()->json($stadium);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Stadium not found.'], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to update stadium.', 'message' => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $stadium = Stadium::findOrFail($id);
            $stadium->delete();

            return response(['message' => 'Stadium deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Stadium not found.'], 404);
        }
    }

    /**
     * Get the stadiums owned by the authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function myStadiums(Request $request)
    {
        $language = $request->header('Accept-Language');

        $user = Auth::user();
        $stadia = $user->stadia()->with(['region', 'activity'])->get();




        $stadiumsData = $stadia->map(function ($stadium) use ($user, $language) {

            // Calculate the average rating and count of reviews
            $ratingData = DB::table('reviews')
                ->select(DB::raw('AVG(rating) AS average_rating'), DB::raw('COUNT(*) AS review_count'))
                ->where('stadium_id', $stadium->id)
                ->first();

            $averageRating = $ratingData->average_rating;
            $reviewCount = $ratingData->review_count;

            return [
                'id' => $stadium->id,
                'name' => $language == 'ar' ? $stadium->name_ar : $stadium->name_en,
                'region_id' => $stadium->region_id,
                'region_name' => $stadium->region ? ($language == 'ar' ? $stadium->region->name_ar : $stadium->region->name_en) : null,
                'activity_id' => $stadium->activity_id,
                'activity_name' => $stadium->activity ? ($language == 'ar' ? $stadium->activity->name_ar : $stadium->activity->name_en) : null,
                'map_url' => $stadium->map_url,
                'description' =>  $language == 'ar' ? $stadium->description_ar : $stadium->description_en,
                'address' =>  $language == 'ar' ? $stadium->address_ar : $stadium->address_en,
                'phone' => $stadium->phone,
                'email' => $stadium->email,
                'image' => $stadium->image,
                'status' => $stadium->status,
                'email' => $stadium->email,
                'email' => $stadium->email,
                'website' => $stadium->website,
                'region' => $stadium->region ? $stadium->region->name : null,
                'is_favorite' => $user->favorites->contains($stadium->id),
                'favorite_id' => $user->favorites()->select('favorites.id')->where('stadium_id', $stadium->id)->value('id'), // Get the favorite ID
                'average_rating' => $averageRating,
                'review_count' => $reviewCount,

            ];
        });

        return response()->json($stadiumsData);
    }

    /**
     * Get the favorite stadiums of the authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function myFavoriteStadiums(Request $request)
    {
        $language = $request->header('Accept-Language');

        $perPage = $request->query('per_page', 10); // Number of records per page, default is 10
        $page = $request->query('page', 1); // Current page number, default is 1


        $user = Auth::user();
        $favoriteStadia = $user->favoriteStadiums()->with(['region', 'activity'])
            ->paginate($perPage, ['*'], 'page', $page);

        $stadiaData = $favoriteStadia->getCollection()->map(function ($stadium) use ($language, $user) {
            $rating = $stadium->reviews->avg('rating');
            $reviewsCount = $stadium->reviews->count();

            return [
                'id' => $stadium->id,
                'name' => $language == 'ar' ? $stadium->name_ar : $stadium->name_en,
                'region_id' => $stadium->region_id,
                'region_name' => $stadium->region ? ($language == 'ar' ? $stadium->region->name_ar : $stadium->region->name_en) : null,
                'activity_id' => $stadium->activity_id,
                'activity_name' => $stadium->activity ? ($language == 'ar' ? $stadium->activity->name_ar : $stadium->activity->name_en) : null,                'map_url' => $stadium->map_url,
                'description' =>  $language == 'ar' ? $stadium->description_ar : $stadium->description_en,
                'address' =>  $language == 'ar' ? $stadium->address_ar : $stadium->address_en,
                'phone' => $stadium->phone,
                'email' => $stadium->email,
                'website' => $stadium->website,
                'image' => $stadium->image,
                'rating' => $rating,
                'reviews_count' => $reviewsCount,
                'is_favorite' => true,
                'favorite_id' =>  $user->favorites()->where('favorites.stadium_id', $stadium->id)->value('favorites.id'),

            ];
        });

        $favoriteStadia->setCollection($stadiaData);

        return response()->json($favoriteStadia);
    }


    public function searchStadia(Request $request)
    {
        $language = $request->header('  ');

        // Retrieve the search parameters from the request
        $name = $request->query('name');
        $activityId = $request->query('activity_id');
        $minRating = $request->query('min_rating');
        $cityId = $request->query('city_id');
        $regionId = $request->query('region_id');


        $perPage = $request->query('per_page', 10); // Number of records per page, default is 10
        $page = $request->query('page', 1); // Current page number, default is 1

        // Start building the query
        $query = Stadium::query();


        if ($name) {
            $query->where(function ($subQuery) use ($name) {
                $subQuery->where('name_ar', 'LIKE', '%' . $name . '%')
                    ->orWhere('name_en', 'LIKE', '%' . $name . '%');
            });
        }

        if ($cityId) {
            $query->whereHas('region.city', function ($q) use ($cityId) {
                $q->where('id', $cityId);
            });
        }

        if ($regionId) {
            $query->where('region_id', $regionId);
        }

        // Apply the filters
        if ($activityId) {
            $query->where('activity_id', $activityId);
        }

        if ($minRating) {
            $query->whereHas('reviews', function ($q) use ($minRating) {
                $q->groupBy('stadium_id')
                    ->havingRaw('AVG(rating) >= ?', [$minRating]);
            });
        }

        // Retrieve the authenticated user
        $user = auth('api')->user();

        if ($user && !$user->isAdmin()) {
            // Add the condition to retrieve only approved stadiums
            $query->where('status', 'Approved');
        }

        $query->paginate($perPage, ['*'], 'page', $page);

        // Retrieve the stadiums with pagination
        $stadia = $query->paginate($perPage, ['*'], 'page', $page);

        // Iterate over the stadiums and add the favorite status with ID
        $stadiumsData = $stadia->getCollection()->map(function ($stadium) use ($user, $language) {
            $isFavorite = $user ?  $user->favorites()->where('favorites.stadium_id', $stadium->id)->exists() : null;
            $rating = $stadium->reviews->avg('rating');
            $reviewsCount = $stadium->reviews->count();

            return [
                'id' => $stadium->id,
                'name' => $language == 'ar' ? $stadium->name_ar : $stadium->name_en,
                'region_id' => $stadium->region_id,
                'activity_id' => $stadium->activity_id,
                'map_url' => $stadium->map_url,
                'description' =>  $language == 'ar' ? $stadium->description_ar : $stadium->description_en,
                'address' =>  $language == 'ar' ? $stadium->address_ar : $stadium->address_en,
                'phone' => $stadium->phone,
                'email' => $stadium->email,
                'website' => $stadium->website,
                'slot' => $stadium->slot,
                'price' => $stadium->price,
                'image' => $stadium->image,
                'status' => $stadium->status,
                'rating' => $rating,
                'reviews_count' => $reviewsCount,
                'is_favorite' => $isFavorite,
                'favorite_id' => $isFavorite ? $user->favorites()->where('favorites.stadium_id', $stadium->id)->value('favorites.id') : null,
            ];
        });

        $stadia->setCollection($stadiumsData);


        return response()->json($stadia);
    }


    public function hasUserBookedStadium($stadiumId)
    {
        $user = Auth::user();

        $hasBooked = $user->bookings()->where('stadium_id', $stadiumId)->exists();

        return response()->json(['has_booked' => $hasBooked]);
    }

    public function changeStatus(Request $request, $id)
    {
        try {
            // Check if the authenticated user is an admin
            $user = Auth::user();
            if (!$user->isAdmin() && !$user->isManager()) {
                return response()->json(['error' => 'You do not have permission to change the status.'], 403);
            }

            // Validate the status value
            $request->validate([
                'status' => 'required|in:Approved,Pending,Rejected',
            ]);

            // Find the stadium by ID
            $stadium = Stadium::findOrFail($id);

            // Update the stadium status
            $stadium->update(['status' => $request->input('status')]);

            // Return the updated stadium data
            return response()->json($stadium);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Stadium not found.'], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to change stadium status.', 'message' => $e->getMessage()], 500);
        }
    }
}
