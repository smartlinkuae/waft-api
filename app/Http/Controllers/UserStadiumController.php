<?php

namespace App\Http\Controllers;

use App\Models\UserStadium;
use App\Http\Requests\StoreUserStadiumRequest;
use App\Http\Requests\UpdateUserStadiumRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UserStadiumController extends Controller
{
    public function index()
    {
        // Retrieve all user_stadium
        $userStadiums = UserStadium::all();
        return response()->json($userStadiums);
    }

    public function store(Request $request)
    {
        try {
            $user = Auth::user();

            if (!$user->isAdmin()) {
                $request['user_id'] = $user->id;
            }

            // Validate the incoming request data
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'stadium_id' => 'required|exists:stadia,id|unique:user_stadia,stadium_id,NULL,id,user_id,' . $request->input('user_id'),
            ]);

            $userStadium = $validatedData;



            // Create a new user_stadium record
            $userStadium = UserStadium::create($userStadium);

            return response()->json($userStadium, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function show(UserStadium $userStadium)
    {
        // Return the specified user_stadium record
        return response()->json($userStadium);
    }

    public function update(Request $request, $id)
    {
        try {
            // Validate the incoming request data
            $validatedData = $request->validate([
                'user_id' => 'required|exists:users,id',
                'stadium_id' => 'required|exists:stadia,id|unique:user_stadia,stadium_id,' . $id . ',id,user_id,' . $request->input('user_id'),
            ]);

            // Find the user_stadium record
            $userStadium = UserStadium::findOrFail($id);

            $userStadiumData = $validatedData;
            $user = Auth::user();

            if (!$user->isAdmin()) {
                $userStadium['user_id'] = $user->id;
            }

            // Update the user_stadium record
            $userStadium->update($userStadiumData);

            return response()->json($userStadium, 200);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'User-Stadium not found.'], 404);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function destroy(UserStadium $userStadium)
    {
        // Delete the specified user_stadium record
        $userStadium->delete();

        return response()->json(null, 204);
    }
}
