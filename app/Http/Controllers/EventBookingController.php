<?php

namespace App\Http\Controllers;

use App\Models\EventBooking;
use App\Http\Requests\StoreEventBookingRequest;
use App\Http\Requests\UpdateEventBookingRequest;

class EventBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEventBookingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventBookingRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventBooking  $eventBooking
     * @return \Illuminate\Http\Response
     */
    public function show(EventBooking $eventBooking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventBooking  $eventBooking
     * @return \Illuminate\Http\Response
     */
    public function edit(EventBooking $eventBooking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEventBookingRequest  $request
     * @param  \App\Models\EventBooking  $eventBooking
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventBookingRequest $request, EventBooking $eventBooking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventBooking  $eventBooking
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventBooking $eventBooking)
    {
        //
    }
}
