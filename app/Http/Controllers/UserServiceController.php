<?php

namespace App\Http\Controllers;

use App\Models\UserService;
use App\Http\Requests\StoreUserServiceRequest;
use App\Http\Requests\UpdateUserServiceRequest;
use Illuminate\Http\Request;

class UserServiceController extends Controller
{
    public function index()
    {
        $userServices = UserService::all();
        return response()->json($userServices);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|exists:users,id',
            'service_id' => 'required|exists:services,id',
        ]);

        $userService = UserService::create($validatedData);
        return response()->json($userService, 201);
    }

    public function show($id)
    {
        $userService = UserService::findOrFail($id);
        return response()->json($userService);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'user_id' => 'exists:users,id',
            'service_id' => 'exists:services,id',
        ]);

        $userService = UserService::findOrFail($id);
        $userService->update($validatedData);
        return response()->json($userService);
    }

    public function destroy($id)
    {
        $userService = UserService::findOrFail($id);
        $userService->delete();
        return response()->json(null, 204);
    }
}
