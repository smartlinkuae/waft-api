<?php

namespace App\Http\Controllers;

use App\Models\MembershipUser;
use App\Http\Requests\StoreMembershipUserRequest;
use App\Http\Requests\UpdateMembershipUserRequest;

class MembershipUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMembershipUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMembershipUserRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MembershipUser  $membershipUser
     * @return \Illuminate\Http\Response
     */
    public function show(MembershipUser $membershipUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MembershipUser  $membershipUser
     * @return \Illuminate\Http\Response
     */
    public function edit(MembershipUser $membershipUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMembershipUserRequest  $request
     * @param  \App\Models\MembershipUser  $membershipUser
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMembershipUserRequest $request, MembershipUser $membershipUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MembershipUser  $membershipUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(MembershipUser $membershipUser)
    {
        //
    }
}
