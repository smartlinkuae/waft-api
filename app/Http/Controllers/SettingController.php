<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only(['store', 'update', 'destroy']);
    }
    public function index()
    {
        $settings = Setting::all();

        return response()->json($settings);
    }

    public function store(Request $request)
    {
        try {
            $data = $request->validate([
                'app_version_android' => 'required|json',
                'app_version_ios' => 'required|json',
                'onboarding' => 'nullable|array',
                'support_phone_number' => 'required|string',
                'privacy_policy' => 'required|string',
                'terms_and_conditions' => 'required|string',
                'about_us' => 'nullable|string',
            ]);

            // Decode the JSON fields
            $data['app_version_android'] = json_decode($data['app_version_android'], true);
            $data['app_version_ios'] = json_decode($data['app_version_ios'], true);

            $setting = Setting::create($data);

            return response()->json($setting, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to create setting', 'message' => $e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $setting = Setting::findOrFail($id);

        return response()->json($setting);
    }

    public function update(Request $request, $id)
    {
        try {
            $setting = Setting::findOrFail($id);

            $data = $request->validate([
                'app_version_android' => 'required|json',
                'app_version_ios' => 'required|json',
                'onboarding' => 'nullable|array',
                'support_phone_number' => 'required|string',
                'privacy_policy' => 'required|string',
                'terms_and_conditions' => 'required|string',
                'about_us' => 'nullable|string',
            ]);

            // Decode the JSON fields
            $data['app_version_android'] = json_decode($data['app_version_android'], true);
            $data['app_version_ios'] = json_decode($data['app_version_ios'], true);

            $setting->update($data);

            return response()->json($setting);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update setting', 'message' => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $setting = Setting::findOrFail($id);
        $setting->delete();

        return response()->json(['message' => 'Setting deleted successfully']);
    }
}
