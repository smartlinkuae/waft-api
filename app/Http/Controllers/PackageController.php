<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PackageController extends Controller
{
    public function index()
    {
        $packages = Package::all();
        return response()->json($packages);
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'name_en' => 'required',
                'name_ar' => 'required',
                'price' => 'numeric|min:0',
                'slot' => 'integer|min:0',
                'type' => 'required|in:subscription,time_slot',
                'slot_type' => 'required_if:slot_type,minutes|in:minutes,hours,days,years',
            ]);

            $package = Package::create($validatedData);

            return response()->json($package, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to save package.', 'message' => $e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $package = Package::findOrFail($id);
        return response()->json($package);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'stadium_id' => 'exists:stadia,id',
                'user_id' => 'exists:users,id',
                'name_en' => 'required',
                'name_ar' => 'required',
                'price' => 'numeric|min:0',
                'slot' => 'integer|min:0',
                'type' => 'required|in:subscription,time_slot',
                'slot_type' => 'required_if:type,time_slot|in:minutes,hours,days,years',
            ]);

            $package = Package::findOrFail($id);
            $package->update($validatedData);

            return response()->json($package);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        $package->delete();

        return response()->json(null, 204);
    }
}
