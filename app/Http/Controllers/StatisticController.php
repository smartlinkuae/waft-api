<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{

    public function getStatistics(Request $request)
    {
        $timeFrame = $request->input('time_frame', 'month'); // Default time frame is 'month'

        // Get the date range based on the selected time frame
        $startDate = $this->getStartDate($timeFrame);
        $endDate = Carbon::now();

        // Get the total number of bookings within the date range
        $totalBookings = Booking::whereBetween('created_at', [$startDate, $endDate])->count();

        // Get the number of paid bookings within the date range
        $paidBookings = Booking::whereHas('payment', function ($query) use ($startDate, $endDate) {
            $query->where('status', 'CAPTURED')
                ->whereBetween('created_at', [$startDate, $endDate]);
        })->count();

        // Get the number of canceled bookings within the date range
        $canceledBookings = Booking::where(function ($query) use ($startDate, $endDate) {
            $query->whereNotNull('cancelled_at')
                ->whereBetween('cancelled_at', [$startDate, $endDate]);
        })->count();

        // Get the total amount of money earned from paid bookings within the date range
        $totalMoney = Payment::where('status', 'CAPTURED')
            ->whereBetween('created_at', [$startDate, $endDate])
            ->sum('amount');

        // Get the total amount of money earned from paid bookings within the date range, grouped by activity
        $totalMoneyByActivity = Payment::join('bookings', 'payments.booking_id', '=', 'bookings.id')
            ->join('stadia', 'bookings.stadium_id', '=', 'stadia.id')
            ->join('activities', 'stadia.activity_id', '=', 'activities.id')
            ->where('payments.status', 'CAPTURED')
            ->whereBetween('payments.created_at', [$startDate, $endDate])
            ->select('activities.id as activity_id', 'activities.name_en', 'activities.name_ar', DB::raw('SUM(payments.amount) as total_money'))
            ->groupBy('activities.id', 'activities.name_en', 'activities.name_ar')
            ->get();

        // Get the number of new players (users with the "user" role) within the date range
        $newPlayers = User::whereHas('role', function ($query) {
            $query->where('name', 'user');
        })->whereBetween('created_at', [$startDate, $endDate])->count();

        // Get the number of users with the most bookings within the date range
        $usersMoreBooked = User::select('users.id', 'users.name', 'users.email', DB::raw('COUNT(bookings.id) as total_bookings'))
            ->leftJoin('bookings', function ($join) use ($startDate, $endDate) {
                $join->on('users.id', '=', 'bookings.user_id')
                    ->whereBetween('bookings.created_at', [$startDate, $endDate]);
            })
            ->groupBy('users.id', 'users.name', 'users.email')
            ->orderByDesc('total_bookings')
            ->limit(5)
            ->get();

        // Top Performing Stadia (most booked)
        $topStadia = Booking::select('stadia.id', 'stadia.name_en', 'stadia.name_ar', DB::raw('count(*) as total_bookings'))
            ->join('stadia', 'bookings.stadium_id', '=', 'stadia.id')
            ->groupBy('stadia.id', 'stadia.name_en', 'stadia.name_ar')
            ->orderByDesc('total_bookings')
            ->limit(5)
            ->get();

        // Popular Time Slots
        $popularTimeSlots = Booking::select('slot', DB::raw('count(*) as total_bookings'))
            ->groupBy('slot')
            ->orderByDesc('total_bookings')
            ->limit(5)
            ->get();

        // Total Booking Count
        $totalBookings = Booking::count();

        // Total Paid Bookings Count
        $totalPaidBookings = Booking::whereHas('payment', function ($query) {
            $query->where('status', 'CAPTURED');
        })->count();

        // Total Canceled Bookings Count
        $totalCanceledBookings = Booking::whereNotNull('cancelled_at')->count();

        // Total Revenue
        $totalRevenue = DB::table('payments')->where('status', 'CAPTURED')->sum('amount');

        // Number of New Users (players)
        $newUserCount = DB::table('users')
            ->where('role_id', '=', 3) // Assuming 3 is the user role ID
            ->whereBetween('created_at', [$startDate, $endDate])
            ->count();

        // Number of Sessions (Subscriptions)
        $totalSessions = Booking::where('type', 'subscription')->count();

        // Average Revenue per Booking
        $averageRevenuePerBooking = $totalPaidBookings > 0 ? $totalRevenue / $totalPaidBookings : 0;

        // Average Sessions per User
        $averageSessionsPerUser = $newUserCount > 0 ? $totalSessions / $newUserCount : 0;

        // Number of Sessions per User (Subscriptions per User)
        $sessionsPerUser = DB::table('users')
            ->select('users.id', 'users.name', 'users.email', DB::raw('COUNT(bookings.id) as total_sessions'))
            ->leftJoin('bookings', function ($join) {
                $join->on('users.id', '=', 'bookings.user_id')
                    ->where('bookings.type', '=', 'subscription');
            })
            ->groupBy('users.id', 'users.name', 'users.email')
            ->get();

        // Stadium Manager with the most stadiums booked
        // Stadium Manager with the most stadiums booked
        $mostBookedStadiumManager = User::select('users.id', 'users.name', 'users.email', DB::raw('count(stadia.id) as total_stadiums_booked'))
            ->join('user_stadia', 'users.id', '=', 'user_stadia.user_id')
            ->join('stadia', 'user_stadia.stadium_id', '=', 'stadia.id')
            ->join('bookings', 'stadia.id', '=', 'bookings.stadium_id')
            ->groupBy('users.id', 'users.name', 'users.email')
            ->orderByDesc('total_stadiums_booked')
            ->first();

        return response()->json([
            'total_bookings' => $totalBookings,
            'paid_bookings' => $paidBookings,
            'canceled_bookings' => $canceledBookings,
            'total_money' => $totalMoney . ' AED',
            'new_players' => $newPlayers,
            'usersMoreBooked' => $usersMoreBooked,
            'top_stadia' => $topStadia,
            'popular_time_slots' => $popularTimeSlots,
            'total_paid_bookings' => $totalPaidBookings,
            'total_canceled_bookings' => $totalCanceledBookings,
            'total_revenue' => $totalRevenue,
            'average_revenue_per_booking' => $averageRevenuePerBooking,
            'average_sessions_per_user' => $averageSessionsPerUser,
            'sessions_per_user' => $sessionsPerUser,
            'total_money_by_activity' => $totalMoneyByActivity,
            'most_booked_stadium_manager' => $mostBookedStadiumManager,

        ]);
    }

    private function getStartDate($timeFrame)
    {
        $startDate = Carbon::now();

        if ($timeFrame === 'month') {
            $startDate->subMonth();
        } elseif ($timeFrame === 'year') {
            $startDate->subYear();
        }

        return $startDate;
    }

    public function stadiumManagerStatistics(Request $request)
    {
        $user = Auth::user();

        // Get all bookings for stadiums owned by the stadium manager
        $stadiums = $user->stadia->pluck('id')->toArray();

        // Get the query parameter for the time period
        $timePeriod = $request->query('time_period', 'all'); // Default to 'all' if not provided

        $query = Booking::whereIn('stadium_id', $stadiums);

        if ($timePeriod === 'last_week') {
            $query->whereBetween('start_time', [Carbon::now()->subWeek(), Carbon::now()]);
        } elseif ($timePeriod === 'last_day') {
            $query->whereBetween('start_time', [Carbon::now()->subDay(), Carbon::now()]);
        } elseif ($timePeriod === 'last_month') {
            $query->whereBetween('start_time', [Carbon::now()->subMonth(), Carbon::now()]);
        } elseif ($timePeriod === 'last_year') {
            $query->whereBetween('start_time', [Carbon::now()->subYear(), Carbon::now()]);
        }

        // Get total bookings for the stadium manager's stadiums based on the time period
        $totalBookings = $query->count();

        // Get completed bookings for the stadium manager's stadiums based on the time period
        $completedBookings = $query->whereNotNull('completed_at')->count();

        // Get canceled bookings for the stadium manager's stadiums based on the time period
        $canceledBookings = $query->whereNotNull('cancelled_at')->count();

        // Get bookings for the current date
        $todayBookings = $query->whereDate('start_time', Carbon::today())->count();

        // Get the total amount from payments for bookings for the stadium manager's stadiums
        $totalAmount = DB::table('bookings')
            ->join('payments', 'bookings.id', '=', 'payments.booking_id')
            ->whereIn('bookings.stadium_id', $stadiums)
            ->where('payments.status', 'CAPTURED')
            ->sum('payments.amount');

        return response()->json([
            'total_bookings' => $totalBookings,
            'completed_bookings' => $completedBookings,
            'canceled_bookings' => $canceledBookings,
            'today_bookings' => $todayBookings,
            'total_amount' => $totalAmount,
        ]);
    }
}
