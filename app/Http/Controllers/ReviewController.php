<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReviewResource;
use App\Models\Review;
use App\Models\Stadium;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('stadiumReviews');
    }
    public function index(Request $request)
    {
        $perPage = $request->query('perPage', 10);
        $page = $request->query('page', 1);

        $reviews = Review::paginate($perPage, ['*'], 'page', $page);

        return response()->json($reviews);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'stadium_id' => 'required|exists:stadia,id',
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string',
        ]);

        $validatedData['user_id'] = Auth::id();

        $review = Review::create($validatedData);

        return response()->json($review, 201);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string',
        ]);

        $review = Review::where('user_id', Auth::id())->findOrFail($id);
        $review->update($validatedData);

        return response()->json($review);
    }

    public function destroy($id)
    {
        $review = Review::where('user_id', Auth::id())->findOrFail($id);
        $review->delete();

        return response()->json(null, 204);
    }


    public function stadiumReviews(Request $request, int $stadiumId)
    {

        try {
            $validator = Validator::make($request->all(), [
                'page' => 'integer|min:1',
                'per_page' => 'integer|min:1',
                'rating' => 'integer|min:1|max:5', // Add rating validation rule
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 422);
            }

            $page = $request->query('page', 1);
            $perPage = $request->query('per_page', 10);
            $rating = $request->query('rating');

            $stadium = Stadium::findOrFail($stadiumId);

            $reviews = $stadium->reviews()
                ->when($rating, function ($query) use ($rating) {
                    // Filter reviews by rating
                    return $query->where('rating', $rating);
                })
                ->orderBy('created_at', 'desc')
                ->with('user') // Eager load user data
                ->paginate($perPage, ['*'], 'page', $page);

            return response()->json($reviews);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Stadium not found.'], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to retrieve stadium reviews.', 'message' => $e->getMessage()], 500);
        }
    }
}
