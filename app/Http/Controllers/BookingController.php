<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Holiday;
use App\Models\Package;
use App\Models\Payment;
use App\Models\Stadium;
use App\Models\WorkTime;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class BookingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('getMostBookedStadiums', 'getAvailableBookingTimes');
        $this->middleware('stadium.status:approved');
    }


    public function index()
    {
        $bookings = Booking::all();
        return response()->json($bookings);
    }

    public function store(Request $request)
    {
        try {
            $user = Auth::user();

            // If not admin ... user_id must be taken by token
            if (!$user->isAdmin()) {
                $request['user_id'] = $user->id;
            }

            $validator = Validator::make($request->all(), [
                'stadium_id' => 'required|exists:stadia,id',
                'package_id' => [
                    'nullable',
                    'exists:packages,id',
                    function ($attribute, $value, $fail) use ($request) {
                        // Retrieve the stadium ID and check if the package ID is valid for that stadium
                        $stadiumId = $request->input('stadium_id');
                        $packageExists = Package::where('id', $value)
                            ->where('stadium_id', $stadiumId)
                            ->exists();

                        if (!$packageExists) {
                            $fail('The selected package is invalid for the specified stadium.');
                        }
                    }
                ],
                'user_id' => 'required|exists:users,id',
                'start_time' => 'required|date',
                'end_time' => 'required|date|after:start_time',
                'type' => 'required|in:subscription,time_slot',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 422);
            }

            $bookingData = $validator->validated();

            // Get the stadium details
            $stadium = Stadium::findOrFail($bookingData['stadium_id']);

            $package = Package::findOrFail($bookingData['package_id']);
            $bookingData['slot'] = $package->slot;
            $bookingData['price'] = $package->price;


            $booking = Booking::create($bookingData);

            return response()->json($booking, 201);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed during Booking', 'message' =>  $e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $booking = Booking::with('stadium', 'package')->findOrFail($id);
        return response()->json($booking);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'stadium_id' => 'exists:stadia,id',
                'package_id' => [
                    'nullable',
                    'exists:packages,id',
                    function ($attribute, $value, $fail) use ($request) {
                        // Retrieve the stadium ID and check if the package ID is valid for that stadium
                        $stadiumId = $request->input('stadium_id');
                        $packageExists = Package::where('id', $value)
                            ->where('stadium_id', $stadiumId)
                            ->exists();

                        if (!$packageExists) {
                            $fail('The selected package is invalid for the specified stadium.');
                        }
                    }
                ],
                'user_id' => 'exists:users,id',
                'start_time' => 'date',
                'end_time' => 'date|after:start_time',
                'type' => 'in:subscription,time_slot',
            ]);

            $booking = Booking::findOrFail($id);

            $bookingData = $validatedData;

            $packageId = $bookingData['package_id'] ?? $booking->package_id;
            $package = Package::findOrFail($packageId);
            $bookingData['slot'] = $package->slot;
            $bookingData['price'] = $package->price;


            $booking->update($bookingData);

            return response()->json($booking);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed Update Booking', 'message' =>  $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $booking = Booking::findOrFail($id);
        $booking->delete();

        return response()->json(null, 204);
    }

    public function getBookingsForStadium($stadiumId, Request $request)
    {
        try {
            $request->validate([
                'per_page' => 'nullable|int',
                'page' => 'nullable|int',
                'sort_by' => 'in:id,start_time,end_time,type,price',
                'sort_order' => 'in:asc,desc',
                'type' => 'nullable|in:time_slot,subscription',
                'status' => 'nullable|in:completed,incomplete,cancelled', // Add status filter
            ]);

            $perPage = $request->query('per_page', 10);
            $page = $request->query('page', 1);

            // Get the sorting column and order from the request
            $sortBy = $request->query('sort_by', 'id'); // Default sorting column is 'id'
            $sortOrder = $request->query('sort_order', 'asc'); // Default sorting order is 'asc'

            // Validate the sorting order to be either 'asc' or 'desc'
            if (!in_array($sortOrder, ['asc', 'desc'])) {
                return response()->json(['error' => 'Invalid sort order. Sort order must be "asc" or "desc".'], 400);
            }

            // Query bookings for the specified stadium
            $query = Booking::with(['user:id,name,email,phone,image', 'package', 'stadium'])
                ->where('stadium_id', $stadiumId);

            // Filter by the 'type' field if provided
            $typeFilter = $request->query('type');
            if ($typeFilter) {
                $query->where('type', $typeFilter);
            }

            // Filter by the 'status' field if provided
            $statusFilter = $request->query('status');
            if ($statusFilter) {
                if ($statusFilter === 'completed') {
                    // Include completed bookings with end_time greater than now
                    $query->whereNotNull('completed_at')
                        ->whereNull('cancelled_at')
                        ->where('end_time', '<', now());
                } elseif ($statusFilter === 'incomplete') {
                    // Include incomplete bookings with end_time greater than now
                    $query->whereNull('completed_at')
                        ->where('end_time', '>', now());
                } elseif ($statusFilter === 'cancelled') {
                    // Include cancelled bookings
                    $query->whereNotNull('cancelled_at');
                }
            }

            $bookings = $query->orderBy($sortBy, $sortOrder)
                ->paginate($perPage, ['*'], 'page', $page);

            return response()->json($bookings);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed To Fetch Bookings', 'message' =>  $e->getMessage()], 500);
        }
    }

    public function getBookingsForStadiumManager(Request $request)
    {
        try {
            $user = Auth::user();

            if (!$user->isStadiumManager()) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            $request->validate([
                'stadium_id' => 'nullable|exists:stadia,id',
                'per_page' => 'nullable|int',
                'page' => 'nullable|int',
                'sort_by' => 'in:id,start_time,end_time,type,price',
                'sort_order' => 'in:asc,desc',
                'type' => 'nullable|in:time_slot,subscription',
                'status' => 'nullable|in:completed,incomplete,cancelled', // Add status filter
            ]);

            $stadiumId = $request->query('stadium_id', null);
            $perPage = $request->query('per_page', 10);
            $page = $request->query('page', 1);

            // Get the sorting column and order from the request
            $sortBy = $request->query('sort_by', 'id'); // Default sorting column is 'id'
            $sortOrder = $request->query('sort_order', 'asc'); // Default sorting order is 'asc'

            // Validate the sorting order to be either 'asc' or 'desc'
            if (!in_array($sortOrder, ['asc', 'desc'])) {
                return response()->json(['error' => 'Invalid sort order. Sort order must be "asc" or "desc".'], 400);
            }

            // Query bookings for the specified stadium owned by the authenticated user
            $query = Booking::with(['user:id,name,email,phone,image', 'package', 'stadium'])
                ->whereHas('stadium.users', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                });


            // Filter by the 'stadium_id' field if provided
            if ($stadiumId) {
                $query->where('bookings.stadium_id', $stadiumId);
            }


            // Filter by the 'type' field if provided
            $typeFilter = $request->query('type');
            if ($typeFilter) {
                $query->where('type', $typeFilter);
            }

            // Filter by the 'status' field if provided
            $statusFilter = $request->query('status');
            if ($statusFilter) {
                if ($statusFilter === 'completed') {
                    // Include completed bookings with end_time greater than now
                    $query->whereNotNull('completed_at')
                        ->whereNull('cancelled_at')
                        ->where('end_time', '<', now());
                } elseif ($statusFilter === 'incomplete') {
                    // Include incomplete bookings with end_time greater than now
                    $query->whereNotNull('completed_at')
                        ->whereNull('cancelled_at')
                        ->where('end_time', '>', now());
                } elseif ($statusFilter === 'cancelled') {
                    // Include cancelled bookings
                    $query->whereNotNull('cancelled_at');
                }
            }

            $bookings = $query->orderBy($sortBy, $sortOrder)
                ->paginate($perPage, ['*'], 'page', $page);

            return response()->json($bookings);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed To Fetch Bookings', 'message' =>  $e->getMessage()], 500);
        }
    }

    public function getMostBookedStadiums(Request $request)
    {
        try {
            $language = $request->header('Accept-Language');

            $request->validate([
                'activity_id' => 'nullable|exists:activities,id',
                'page' => 'required|integer|min:1',
                'per_page' => 'required|integer|min:1',
            ]);
            $page = $request->query('page', 1); // Current page number, default is 1
            $perPage = $request->query('per_page', 10); // Number of records per page, default is 10

            $activityId = $request->input('activity_id');

            $user = auth('api')->user(); // Assuming you have user authentication set up

            $query = Stadium::whereHas('bookings', function ($query) use ($activityId) {
                if ($activityId)
                    $query->where('activity_id', $activityId);
            })
                ->withCount(['bookings' => function ($query) use ($activityId) {
                    if ($activityId)
                        $query->where('activity_id', $activityId);
                }])
                ->orderBy('bookings_count', 'desc');

            // Paginate the results
            $stadia = $query->paginate($perPage, ['*'], 'page', $page);

            $stadiumsData = $stadia->getCollection()->map(function ($stadium) use ($user, $language) {
                $isFavorite = $user ? $user->favorites()->where('favorites.stadium_id', $stadium->id)->exists() : null;
                $favoriteId = $isFavorite ? $user->favorites()->where('favorites.stadium_id', $stadium->id)->value('favorites.id') : null;
                $rating = $stadium->reviews->avg('rating');
                $reviewsCount = $stadium->reviews->count();

                return [
                    'id' => $stadium->id,
                    'name' => $language == 'ar' ? $stadium->name_ar : $stadium->name_en,
                    'region_id' => $stadium->region_id,
                    'activity_id' => $stadium->activity_id,
                    'map_url' => $stadium->map_url,
                    'description' =>  $language == 'ar' ? $stadium->description_ar : $stadium->description_en,
                    'address' =>  $language == 'ar' ? $stadium->address_ar : $stadium->address_en,
                    'phone' => $stadium->phone,
                    'email' => $stadium->email,
                    'website' => $stadium->website,
                    'image' => $stadium->image,
                    'slot' => $stadium->slot,
                    'price' => $stadium->price,
                    'rating' => $rating,
                    'reviewsCount' => $reviewsCount,
                    'bookings_count' => $stadium->bookings_count,
                    'is_favorite' => $isFavorite,
                    'favorite_id' => $favoriteId,
                ];
            });
            $stadia->setCollection($stadiumsData);

            return response()->json($stadia);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to retrieve Most Booked Stadium'], 500);
        }
    }

    public function getUserBookings(Request $request)
    {
        try {
            $request->validate([
                'type' => 'nullable|in:time_slot,subscription',
            ]);
            $userId = Auth::id();
            $perPage = $request->query('per_page', 10);
            $page = $request->query('page', 1);
            $type = $request->query('type');


            $bookings = Booking::with(['package', 'stadium', 'cancelledBy'])
                ->where('user_id', $userId);

            if ($type)
                $bookings =  $bookings->where('type', $type);

            $bookings = $bookings->paginate($perPage, ['*'], 'page', $page);

            return response()->json($bookings);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Booking not found.', 'message' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to retrieve User Booking', 'message' => $e->getMessage()], 500);
        }
    }

    // Condation: Payment process Complete succssfully &&  Booking Time Finish
    public function getUserCompletedBookings(Request $request)
    {
        try {
            $request->validate([
                'type' => 'nullable|in:time_slot,subscription',
            ]);

            $userId = Auth::id();
            $perPage = $request->query('per_page', 10);
            $page = $request->query('page', 1);
            $type = $request->query('type');

            $completedBookings = Booking::with(['package', 'stadium'])
                ->where('user_id', $userId)
                ->whereNotNull('completed_at')
                ->where('end_time', '<', now());

            if ($type)
                $completedBookings =  $completedBookings->where('type', $type);

            $completedBookings = $completedBookings->paginate($perPage, ['*'], 'page', $page);

            return response()->json($completedBookings);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Booking not found.', 'message' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to retrieve User Booking', 'message' => $e->getMessage()], 500);
        }
    }

    // Get upcoming bookings for the authenticated user
    public function getUpcomingBookings(Request $request)
    {
        $userId = Auth::id();
        $perPage = $request->query('per_page', 10);
        $page = $request->query('page', 1);
        $type = $request->query('type');

        $upcomingBookings = Booking::with(['package', 'stadium'])
            ->where('user_id', $userId)
            ->whereNotNull('completed_at')
            ->whereNull('cancelled_at')
            ->where('end_time', '>', now());
        if ($type)
            $upcomingBookings =  $upcomingBookings->where('type', $type);

        $upcomingBookings = $upcomingBookings->paginate($perPage, ['*'], 'page', $page);

        return response()->json($upcomingBookings);
    }


    public function cancel(Request $request, $id)
    {
        $booking = Booking::findOrFail($id);
        $user = auth('api')->user();
        try{

            $now = Carbon::now();
            $bookingId = $booking->id;
            $booking->cancelled_at = $now;
            $booking->cancelled_by = Auth::id();
            $booking->save();

            // Refund
            $paymentController = new PaymentController();
            $paymentData = [
                "booking_id" => $bookingId,
                "reason" => $booking->cancel_reason ?? 'No Reason',
            ];
            $cancellationTimeFrame = 30; // minutes, adjust as needed
            if ($now->diffInMinutes($booking->start_time) < $cancellationTimeFrame) {
                return response()->json(['message' => 'You can only cancel the booking within 1 hours before the start time.'], 403);
            }

                // Send notification for booking cancellation
                $notificationController = new NotificationController();
                $bookingId = $booking->id;

                // Authorization checks
                if ($user->isAdmin()) {
                    // Admin can cancel any booking
                    $booking->cancel_reason = $request->input('reason') ?? 'cancelled by admin';
                    $booking->save();
                    $responseRefundPayment = $paymentController->refundHandle(new Request($paymentData));

                    if( $responseRefundPayment->status() / 100 == 2){
                        $notificationData = [
                            "user_ids" => [$booking->user_id],
                            "title" => "Booking Canceled By Admin",
                            "body" => "Your booking with ID: $bookingId has been canceled.",
                        ];
                        $notificationController->sendNotificationWithStore(new Request($notificationData));
                    }else{
                         return response()->json(["error" => "Failed", "message" => $responseRefundPayment], 500);
                    }

                } elseif ($user->isStadiumManager()){


                        // Stadium manager can cancel bookings only for the stadiums they manage
                        // All all stadiums for this user
                        $stadiums = $user->stadia()->pluck('stadia.id')->toArray();
                        if (!in_array($booking->stadium_id, $stadiums) && $booking->user_id !== $user->id) {
                            return response()->json(['error' => 'You are not authorized to cancel this booking.'], 401);
                        }
                        $responseRefundPayment = $paymentController->refundHandle(new Request($paymentData));

                        if( $responseRefundPayment->status() / 100 == 2){
                            $booking->cancel_reason = $request->input('reason');
                            $booking->save();

                            $notificationData = [
                                "user_ids" => [$booking->user_id],
                                "title" => "Booking Canceled By Stadium Manager",
                                "body" => "Your booking with ID: $bookingId has been canceled.",
                            ];
                            $notificationController->sendNotificationWithStore(new Request($notificationData));
                        }else{
                            return response()->json(["error" => "Failed", "message" => $responseRefundPayment], 500);
                        }
                    } else {

                        // Check if the booking belongs to the authenticated user
                        if ($booking->user_id !== $user->id) {
                            return response()->json(['error' => 'Unauthorized'], 401);
                        }

                        $responseRefundPayment = $paymentController->refundHandle(new Request($paymentData));

                        if( $responseRefundPayment->status() / 100 == 2){
                            $booking->cancel_reason = $request->input('reason');
                            $booking->save();

                            $notificationData = [
                                "user_ids" => [$booking->user_id],
                                "title" => "Booking Canceled",
                                "body" => "Your booking with ID: $bookingId has been canceled.",
                            ];
                            $notificationController->sendNotificationWithStore(new Request($notificationData));
                        }else{
                            return response()->json(["error" => "Failed", "message" => $responseRefundPayment], 500);
                        }
                    }


                return response()->json(['message' => 'Booking has been canceled successfully.', 'responseRefund' => $responseRefundPayment]);


        }catch(\Exception $e){
            $booking->cancelled_at = null;
            $booking->cancelled_by = null;
            $booking->cancel_reason = null;
            $booking->save();

            return response()->json(["error" => "Failed", "message" => $e->getMessage()], 500);
        }
    }

    public function getCancelledBookings(Request $request)
    {
        $perPage = $request->query('per_page', 10);
        $page = $request->query('page', 1);

        $userCancelledBookings = Booking::with(['package', 'stadium', 'cancelledBy'])->where('user_id', Auth::id())
            ->whereNotNull('cancelled_at')
            ->paginate($perPage, ['*'], 'page', $page);

        return response()->json($userCancelledBookings);
    }


    // Get Available Booking Times For Specified Date
    public function getAvailableBookingTimes(Request $request)
    {
        try {
            $request->validate([
                "stadium_id" => "required|exists:stadia,id",
                "package_id" => "required|exists:packages,id",
                "date" => "required|date",
            ]);


            $stadiumId = $request->input("stadium_id");
            $packageId = $request->input("package_id");

            $date = Carbon::parse($request->input("date"))->toDateString();
            $currentTime = Carbon::now()->addHours(4); // UAE GMT +4

            $stadium = Stadium::with(["packages" => function ($query) use ($packageId) {
                $query->where("type", "time_slot")->where("id", $packageId);
            }])->findOrFail($stadiumId);

            if ($stadium->packages->isEmpty()) {
                return response()->json(["error" => "Package not found."], 404);
            }

            $package = $stadium->packages->first();
            $slot = $package->slot;

            // Get work times for the stadium on the specified day
            $workTimes = WorkTime::where("stadium_id", $stadiumId)
                ->where("day", Carbon::parse($date)->englishDayOfWeek)
                ->get();

            // Get bookings for the stadium on the specified date
            $bookings = Booking::where("stadium_id", $stadiumId)
                ->whereDate("start_time", $date)
                ->whereNull("cancelled_at")
                ->get();
            // Get holidays for the stadium on the specified date
            $holidays = Holiday::where("stadium_id", $stadiumId)
                ->whereDate("date", $date)
                ->get();

            // Calculate available booking times based on work times, bookings, and holidays
            $availableTimes = [];

            foreach ($workTimes as $workTime) {
                $startTime = Carbon::parse($date . " " . $workTime->start_time);
                $endTime = Carbon::parse($date . " " . $workTime->end_time);
                $time = clone $startTime;

                if ($slot > 0) { //This condition to avoid an infinite loop if the slot was 0 will repeat while loop without $time var increase while
                    while ($time < $endTime) {
                        // Log::info("time .........." . $time);
                        // Check if the time is after the current time
                        if ($time->gt($currentTime)) {
                            $isAvailable = true;
                            // Check if the time falls within a booked slot
                            foreach ($bookings as $booking) {
                                $bookingStartTime = Carbon::parse($booking->start_time);
                                $bookingEndTime = Carbon::parse($booking->end_time);

                                if ($time >= $bookingStartTime && $time < $bookingEndTime && $booking->completed_at != null ) {
                                    $isAvailable = false;
                                    break;
                                }
                            }
                            // Check if the time falls within a holiday
                            foreach ($holidays as $holiday) {
                                $holidayDate = Carbon::parse($holiday->date);

                                if ($time->isSameDay($holidayDate)) {
                                    $isAvailable = false;
                                    break;
                                }
                            }

                            if ($isAvailable) {
                                $availableTimes[] = [
                                    "time" => $time->format("H:i"),
                                    "status" => "available",
                                ];
                            } else {
                                $availableTimes[] = [
                                    "time" => $time->format("H:i"),
                                    "status" => "booked",
                                ];
                            }
                        }

                        $time->addMinutes($slot);
                    }
                }
            }

            return response()->json($availableTimes);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(["errors" => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(["error" => "Stadium not found.", "message" => $e->getMessage()], 404);
        } catch (\Exception $e) {
            // Log the error message for debugging purposes
            //Log::error($e->getMessage());

            // Handle other exceptions
            return response()->json(["error" => "Failed to retrieve Available Booking Times", "message" => $e->getMessage()], 500);
        }
    }


    // Get Available Booking Times For Specified Month
    public function getAvailableBookingTimesInMonth(Request $request)
    {
        try {
            $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'month' => 'required|date_format:Y-m',
            ]);

            $stadiumId = $request->input('stadium_id');
            $month = Carbon::parse($request->input('month'));

            $stadium = Stadium::findOrFail($stadiumId);

            // Get the start and end dates of the month
            $startDate = $month->startOfMonth()->toDateString();
            $endDate = $month->endOfMonth()->toDateString();

            // Get bookings for the stadium within the month
            $bookings = Booking::where('stadium_id', $stadiumId)
                ->where('start_time', '>=', $startDate)
                ->where('end_time', '<=', $endDate)
                ->get();

            // Get holidays for the stadium within the month
            $holidays = Holiday::where('stadium_id', $stadiumId)
                ->where('date', '>=', $startDate)
                ->where('date', '<=', $endDate)
                ->get();

            // Calculate available booking times based on work times, bookings, and holidays
            $calendar = [];
            $currentDate = Carbon::parse($startDate)->copy();

            while ($currentDate <= $endDate) {
                $day = [
                    'date' => $currentDate->format('Y-m-d'),
                    'availableTimes' => []
                ];

                $workTimes = WorkTime::where('stadium_id', $stadiumId)
                    ->where('day', strtolower($currentDate->englishDayOfWeek))
                    ->get();

                foreach ($workTimes as $workTime) {
                    $startTime = Carbon::parse($currentDate->format('Y-m-d') . ' ' . $workTime->start_time);
                    $endTime = Carbon::parse($currentDate->format('Y-m-d') . ' ' . $workTime->end_time);

                    $time = clone $startTime;

                    while ($time < $endTime) {
                        $isAvailable = true;

                        // Check if the time falls within a booked slot
                        foreach ($bookings as $booking) {
                            $bookingStartTime = Carbon::parse($booking->start_time);
                            $bookingEndTime = Carbon::parse($booking->end_time);

                            if ($time >= $bookingStartTime && $time < $bookingEndTime) {
                                $isAvailable = false;
                                break;
                            }
                        }

                        // Check if the time falls within a holiday
                        foreach ($holidays as $holiday) {
                            $holidayDate = Carbon::parse($holiday->date);

                            if ($time->isSameDay($holidayDate)) {
                                $isAvailable = false;
                                break;
                            }
                        }

                        if ($isAvailable) {
                            $day['availableTimes'][] = $time->format('H:i');
                        }

                        $time->addMinutes($stadium->slot);
                    }
                }

                $calendar[] = $day;
                $currentDate->addDay();
            }

            return response()->json($calendar);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Stadium not found.'], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to retrieve Available Booking Times'], 500);
        }
    }
}
