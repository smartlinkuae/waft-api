<?php

namespace App\Http\Controllers;

use App\Models\Stadium;
use App\Models\StadiumImage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class StadiumImageController extends Controller
{
    public function index($stadiumId)
    {
        $stadium = Stadium::findOrFail($stadiumId);
        $images = $stadium->images;

        return response()->json(['stadium_images' => $images]);
    }

    public function store(Request $request, $stadiumId)
    {
        try {
            $request->validate([
                'images.*' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            ]);

            $stadium = Stadium::findOrFail($stadiumId);

            $images = [];


            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $image) {
                    // Generate a unique filename based on the current date and a random number
                    $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $image->getClientOriginalExtension();

                    // Move the uploaded image to the storage directory with the new filename
                    $imagePath = $image->storeAs('images/stadia', $imageName, 'public');
                    $imageURL = url('storage/' . $imagePath);

                    // Save the image path to the database
                    $images[] = ['image_path' => $imageURL];
                }
            }

            // Insert the images to the database using the stadium's relationship
            $stadium->images()->createMany($images);

            return response()->json(['message' => 'Stadium images uploaded successfully.']);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to upload stadium images', 'message' => $e->getMessage()], 500);
        }
    }

    public function destroy($stadiumId, $imageId)
    {
        try {
            $stadium = Stadium::findOrFail($stadiumId);

            $image = $stadium->images()->findOrFail($imageId);

            // Delete the image from the storage
            Storage::disk('public')->delete(str_replace('storage/', '', $image->image_path));

            // Delete the image record from the database
            $image->delete();

            return response()->json(['message' => 'Stadium image deleted successfully']);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Stadium-image not found.'], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to delete Stadium image.', 'message' => $e->getMessage()], 500);
        }
    }
}
