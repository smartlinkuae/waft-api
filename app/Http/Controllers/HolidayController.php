<?php

namespace App\Http\Controllers;

use App\Models\Holiday;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class HolidayController extends Controller
{
    public function index()
    {
        $holidays = Holiday::all();
        return response()->json($holidays);
    }

    public function store(Request $request)
    {
        // TODO: - ADMIN ROLE OR Stadium Owner
        try {
            // Validate request data
            $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'name_en' => 'required',
                'name_ar' => 'required',
                'date' => 'required|date'
            ]);

            $holiday = Holiday::create($request->all());
            return response()->json($holiday, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function show($id)
    {
        $holiday = Holiday::findOrFail($id);
        return response()->json($holiday);
    }

    public function update(Request $request, $id)
    {
        try {
            // Validate request data
            $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'name_en' => 'required',
                'name_ar' => 'required',
                'date' => 'required|date'
            ]);

            // TODO: - ADMIN ROLE OR Stadium Owner
            $holiday = Holiday::findOrFail($id);
            $holiday->update($request->all());
            return response()->json($holiday, 200);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function destroy($id)
    {
        // TODO: - ADMIN ROLE OR Stadium Owner
        $holiday = Holiday::findOrFail($id);
        $holiday->delete();
        return response()->json(null, 204);
    }
}
