<?php

namespace App\Http\Controllers;

use App\Models\SpecialEvent;
use App\Http\Requests\StoreSpecialEventRequest;
use App\Http\Requests\UpdateSpecialEventRequest;

class SpecialEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSpecialEventRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSpecialEventRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SpecialEvent  $specialEvent
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialEvent $specialEvent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SpecialEvent  $specialEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecialEvent $specialEvent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSpecialEventRequest  $request
     * @param  \App\Models\SpecialEvent  $specialEvent
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSpecialEventRequest $request, SpecialEvent $specialEvent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SpecialEvent  $specialEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialEvent $specialEvent)
    {
        //
    }
}
