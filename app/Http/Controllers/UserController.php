<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PassportAuthController;
use App\Http\Middleware\CheckAdmin;
use App\Http\Resources\UserBriefResource;
use App\Mail\VerificationCodeMail;
use App\Mail\VerificationSuccessMail;
use App\Models\EmailUpdate;
use App\Models\Role;
use App\Models\User;
use App\Models\VerificationCode;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Kreait\Firebase\Factory;
use Kreait\Laravel\Firebase\Facades\Firebase;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(CheckAdmin::class)->only(['index', 'store', 'show', 'update', 'destroy']);
    }

    public function index(Request $request)
    {
        try {
            $request->validate([
                'per_page' => 'nullable|int',
                'page' => 'nullable|int',
                'role' => 'in:admin,stadium_manager,user',
                'sort_order' => 'in:asc,desc',
                'search_value' => 'nullable|string',
            ]);


            $perPage = $request->query('per_page', 10);
            $page = $request->query('page', 1);
            $roleName = $request->query('role');


            // Get the sorting column and order from the request
            $sortBy = $request->query('sort_by', 'id'); // Default sorting column is 'id'
            $sortOrder = $request->query('sort_order', 'asc'); // Default sorting order is 'asc'
            $searchValue = $request->query('search_value');



            // Query the users with their roles based on the role name filter
            $usersQuery = User::query();

            if ($roleName) {
                // Filter users by role name
                $usersQuery->whereHas('role', function ($query) use ($roleName) {
                    $query->where('name', $roleName);
                });
            }

            // Define the array of columns to search in
            $searchColumns = ['name', 'email', 'phone'];

            if ($searchValue) {
                $usersQuery->where(function ($query) use ($searchValue, $searchColumns) {
                    foreach ($searchColumns as $column) {
                        $query->orWhere($column, 'LIKE', '%' . $searchValue . '%');
                    }
                });
            }
            // Paginate the filtered results
            $users = $usersQuery
                ->orderBy($sortBy, $sortOrder)
                ->paginate($perPage, ['*'], 'page', $page);

            // Format the paginated user data using the UserBriefResource
            $formattedUsers = UserBriefResource::collection($users);

            return $formattedUsers;
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed To Fetch All', 'message' =>  $e->getMessage()], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            // Begin a database transaction
            DB::beginTransaction();

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'phone' => ['nullable', 'regex:/^(\\+|00)(\\d{4}[0-9]*)$/i', 'unique:users'],
                'password' => 'required|min:3',
                'birthday' => 'nullable|date',
                'gender' => 'nullable|integer|between:1,2',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
                'role' => 'nullable|exists:roles,name' // New validation rule for the role name
            ]);

            $user = new \App\Models\User();

            $user->name         = $request->name;
            $user->email        = $request->email;
            $user->phone        = $request->phone;
            $user->birthday     = $request->birthday;
            $user->gender       = $request->gender;
            $user->password     = bcrypt($request->password);
            $role_name          = $request->role ?? 'user';

            // Upload the profile image if provided
            if ($request->hasFile('image')) {
                // Generate a unique filename based on the current date and a random number
                $filename = date('Y-m-d') . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $request->file('image')->storeAs('images/profile', $filename, 'public');

                // Update the user's image with the full path
                $user->image = url('storage/images/profile/' . $filename);
            }

            $user->save();

            // Assign a role to the user based on the role name (if provided)
            $role = Role::where('name', $role_name)->first();
            if ($role) {
                $user->role_id = $role->id;
                $user->save();
            }

            // Generate and send the verification code to the user's email
            // $passportAuthController = new PassportAuthController();
            // $passportAuthController->sendVerificationCode($user);


            // if you want to enable direct login enable the next
            $token = $user->createToken('auth_Token_Khaled_Stadium')->accessToken;

            // Format the user data using the UserBriefResource
            $formattedUser = new UserBriefResource($user);

            // Commit the transaction as all database operations are successful up to this point
            DB::commit();

            return response([
                'user' => $formattedUser,
                'token' => $token
            ], Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            DB::rollBack();
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            DB::rollBack();
            return response(['error' => 'An error occurred during registration.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function show(int $id)
    {
        try {
            $user = user::findOrFail($id);

            // Retrieve the role name using the role_id
            $userRole = Role::find($user->role_id);
            $roleName = $userRole ? $userRole->name : null;
            $user->role =  $roleName;

            // Unset the password field
            unset($user->password);


            return response()->json($user);
        } catch (\Exception $e) {
            return response()->json(['error' => 'User not found.'], 404);
        }
    }

    public function update(Request  $request, int $id)
    {
        try {
            // Retrieve the authenticated user
            $user = User::findOrFail($id);

            // Validate the request
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $user->id,
                'phone' => ['nullable', 'regex:/^(\\+|00)(\\d{4}[0-9]*)$/i', 'unique:users,phone,' . $user->id],
                'birthday' => 'nullable|date',
                'gender' => 'nullable|integer|between:1,2',
                //'image' => 'nullable|image|max:2048',
                'role' => 'nullable|exists:roles,name' // New validation rule for the role name

            ]);

            // Update the user profile
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->birthday = $request->input('birthday');
            $user->gender = $request->input('gender');

            $role_name = $request->role ?? 'user';

            // Handle the uploaded image
            if ($request->hasFile('image')) {
                $oldImagePath = $user->image;

                // Generate a unique filename based on the current date and a random number
                $filename = date('Y-m-d') . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $request->file('image')->storeAs('images/profile', $filename, 'public');

                // Update the user's image with the full path
                $user->image = url('storage/images/profile/' . $filename);

                // Delete the old image if it exists
                if (!empty($oldImagePath) && Storage::disk('public')->exists($oldImagePath)) {
                    Storage::disk('public')->delete($oldImagePath);
                }
            }

            $user->save();

            // Assign a role to the user based on the role name (if provided)
            $role = Role::where('name', $role_name)->first();
            if ($role) {
                $user->role_id = $role->id;
                $user->save();
            }

            // Format the user data using the UserBriefResource
            $formattedUser = new UserBriefResource($user);

            // Prepare the response data
            $responseData = [
                'message' => 'Profile updated successfully',
                'user' => $formattedUser,
            ];

            return response()->json($responseData);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update user', 'message' => $e->getMessage()], 500);
        }
    }


    public function destroy(int $id)
    {
        try {
            $user = user::findOrFail($id);
            $user->delete();

            return response(['message' => 'User deleted successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'User not found.'], 404);
        }
    }

    public function deleteAccount(Request $request)
    {
        $user = Auth::user();

        // Validate the password provided by the user
        if (!Hash::check($request->input('password'), $user->password)) {
            throw ValidationException::withMessages([
                'password' => 'Invalid password',
            ]);
        }

        // Perform any necessary cleanup or additional actions before deleting the account
        /// Set the email_verified_at column to null
        $user->email_verified_at = null;
        $user->role_id = null;
        $user->firebase_token = null;


        // Save the changes
        $user->save();
        // Delete the user account
        $user->delete();

        return response()->json(['message' => 'Account deleted successfully']);
    }




    public function updateProfile(Request $request)
    {
        try {
            // Retrieve the authenticated user
            $user = Auth::user();
            // Validate the request

            $request->validate([
                'name' => 'required',
                //'email' => 'required|email|unique:users,email,' . $user->id,
                'phone' => ['nullable', 'regex:/^(\\+|00)(\\d{4}[0-9]*)$/i', 'unique:users,phone,' . $user->id],
                'birthday' => 'nullable|date',
                'gender' => 'nullable|integer|between:1,2',
                //'image' => 'nullable|image|max:2048',
                'browsing_mode' => 'nullable|in:user,stadium_manager',
                'role' => [
                    'nullable',
                    Rule::in(Role::where('name', 'user')->orWhere('name', 'stadium_manager')->pluck('name')->all()),
                ],
            ]);

            // Update the user profile
            $user->name = $request->input('name');
            // $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->birthday = $request->input('birthday');
            $user->gender = $request->input('gender');
            $user->browsing_mode = $request->browsing_mode;

            // Only update the role if it's a user role or stadium manager
            $roleName = $request->input('role');
            if (in_array($roleName, Role::where('name', 'user')->orWhere('name', 'stadium_manager')->pluck('name')->all())) {
                $role = Role::where('name', $roleName)->first();
                $user->role_id = $role->id;
            }

            // Handle the uploaded image
            if ($request->hasFile('image')) {
                $oldImagePath = $user->image;

                // Generate a unique filename based on the current date and a random number
                $filename = date('Y-m-d') . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $request->file('image')->storeAs('images/profile', $filename, 'public');

                // Update the user's image with the full path
                $user->image = url('storage/images/profile/' . $filename);

                // Delete the old image if it exists
                if (!empty($oldImagePath) && Storage::disk('public')->exists($oldImagePath)) {
                    Storage::disk('public')->delete($oldImagePath);
                }
            }

            $user->save();

            // Prepare the response data
            $responseData = [
                'message' => 'Profile updated successfully',
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'birthday' => $user->birthday,
                    'gender' => $user->gender,
                    'email_verified_at' => $user->email_verified_at,
                    'image' => $user->image,
                    'role_id' => $user->role_id,
                    'role' => $roleName,
                    'firebase_token' => $user->firebase_token,
                    'browsing_mode' => $user->browsing_mode,
                ],
            ];

            return response()->json($responseData);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update profile', 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * Send OTP (One-Time Password) to the user's email for updating the email address.
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendUpdateEmailOTP(Request $request)
    {
        try {
            $request->validate([
                'new_email' => 'required|email|unique:users,email',
            ]);

            $new_email = $request->input('new_email');

            // Retrieve the authenticated user
            $user = Auth::user();

            // Generate the verification code
            $verificationCode = rand(100000, 999999);

            // Store the verification code and new_email in the email_updates table
            EmailUpdate::create([
                'user_id' => $user->id,
                'new_email' => $new_email,
                'code' => $verificationCode,
            ]);

            // Send the verification code to the user's new email
            Mail::to($new_email)->send(new VerificationCodeMail($verificationCode));

            return response(['message' => 'OTP sent successfully.'], Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response(['error' => 'An error occurred while sending OTP.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateEmail(Request $request)
    {
        try {
            $user = Auth::user();

            $request->validate([
                'new_email' => 'required|email|unique:users,email',
                'verification_code' => 'required|digits:6',
            ]);

            $new_email = $request->input('new_email');
            $verificationCode = $request->input('verification_code');

            // Retrieve the latest verification code and new_email from the email_updates table
            $latestCode = EmailUpdate::where('user_id', $user->id)
                ->where('new_email', $new_email)
                ->where('code', $verificationCode)
                ->where('used', false)
                ->orderBy('created_at', 'desc')
                ->first();

            if (!$latestCode) {
                return response([
                    'error' => 'Invalid verification code for the new email.',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Update the user's email in the users table
            $user->email = $new_email;
            $user->save();

            // Mark the verification code as used
            $latestCode->used = true;
            $latestCode->save();

            // Send the verification success email to the new email
            Mail::to($user->email)->send(new VerificationSuccessMail());

            return response(['message' => 'Email updated successfully.'], Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response(['error' => 'An error occurred while updating the email.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
