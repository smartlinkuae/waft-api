<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class CityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
        $this->middleware('admin')->only(['store', 'update', 'destroy']);
    }

    public function index()
    {
        $cities = City::all();
        return response()->json($cities);
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required|unique:cities|max:255',
                'name_ar' => 'required|unique:cities|max:255',
                'country_id' => 'required|exists:countries,id',
            ]);

            $city = City::create($validatedData);

            return response()->json($city, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function show($id)
    {
        $city = City::with('regions')->findOrFail($id);
        return response()->json($city);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => [
                    'required',
                    'max:255',
                    Rule::unique('cities')->ignore($id),
                ],
                'name_ar' => [
                    'required',
                    'max:255',
                    Rule::unique('cities')->ignore($id),
                ],
                'country_id' => 'required|exists:countries,id',
            ]);

            $city = City::findOrFail($id);
            $city->update($validatedData);

            return response()->json($city);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        return response()->json(null, 204);
    }
}
