<?php

namespace App\Http\Controllers;

use App\Models\StadiumActivity;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StadiumActivityController extends Controller
{
    public function index()
    {
        $stadiumActivities = StadiumActivity::all();

        return response()->json($stadiumActivities);
    }

    public function show($id)
    {
        try {
            $stadiumActivity = StadiumActivity::findOrFail($id);
            return response()->json($stadiumActivity);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Stadium Activity not found.'], 404);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'activity_id' => 'required|exists:activities,id',
            ]);

            $stadiumActivity = StadiumActivity::create($request->all());

            return response()->json($stadiumActivity, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to create Stadium Activity.', 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'activity_id' => 'required|exists:activities,id',
            ]);

            $stadiumActivity = StadiumActivity::findOrFail($id);
            $stadiumActivity->update($request->all());

            return response()->json($stadiumActivity);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update Stadium Activity.'], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $stadiumActivity = StadiumActivity::findOrFail($id);
            $stadiumActivity->delete();

            return response()->json(null, 204);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Stadium Activity not found.'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to delete Stadium Activity.', 'message' => $e->getMessage()], 500);
        }
    }
}
