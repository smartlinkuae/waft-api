<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
        $this->middleware('admin')->only(['store', 'update', 'destroy']);

    }

    public function index()
    {
        $activities = Activity::all();
        return response()->json($activities);
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required|string|max:255',
                'name_ar' => 'required|string|max:255',
                'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', // Add image validation rules
            ]);

            $activityData = $validatedData;

            // Handle image upload
            if ($request->hasFile('image')) {
                // Generate a unique filename based on the current date and a random number
                $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $imagePath = $request->file('image')->storeAs('images/activity', $imageName, 'public');
                $imageURL = url('storage/' . $imagePath);

                $activityData['image'] = $imageURL;
            }


            $activity = Activity::create($activityData);

            return response()->json($activity, 201);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to Save Activity.', 'message' => $e->getMessage()], 500);
        }
    }

    public function show($id)
    {
        $activity = Activity::find($id);

        if (!$activity) {
            return response()->json(['message' => 'Activity not found'], 404);
        }

        return response()->json($activity);
    }

    public function update(Request $request, $id)
    {
        try {
            $validatedData = $request->validate([
                'name_en' => 'required|string|max:255',
                'name_ar' => 'required|string|max:255',
                'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', // Add image validation rules
            ]);


            $activity = Activity::findOrFail($id);
            $activityData = $validatedData;

            // Handle image upload
            if ($request->hasFile('image')) {
                $oldImagePath = $activity->image;


                // Generate a unique filename based on the current date and a random number
                $imageName = time() . '_' . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                // Move the uploaded image to the storage directory with the new filename
                $imagePath = $request->file('image')->storeAs('images/activity', $imageName, 'public');
                $imageURL = url('storage/' . $imagePath);

                $activityData['image'] = $imageURL;

                // Delete the old image if it exists
                if (!empty($oldImagePath) && Storage::disk('public')->exists($oldImagePath)) {
                    Storage::disk('public')->delete($oldImagePath);
                }
            }

            $activity->update($activityData);
            return response()->json($activity);
        } catch (ValidationException $e) {
            // Handle validation errors
            return response()->json(['errors' => $e->errors()], 422);
        } catch (ModelNotFoundException $e) {
            // Handle model not found error
            return response()->json(['error' => 'Activity not found.'], 404);
        } catch (\Exception $e) {
            // Handle other exceptions
            return response()->json(['error' => 'Failed to update Activity.', 'message' => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $activity = Activity::find($id);

        if (!$activity) {
            return response()->json(['message' => 'Activity not found'], 404);
        }

        $activity->delete();

        return response()->json(['message' => 'Activity deleted successfully']);
    }
}
