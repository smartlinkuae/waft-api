<?php

namespace App\Http\Controllers;

use App\Models\WorkTime;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WorkTimeController extends Controller
{
    public function index()
    {
        $workTimes = WorkTime::all();
        return response()->json($workTimes);
    }

    public function store(Request $request)
    {
        // TODO: - ADMIN ROLE OR Stadium Owner

        try {
            // Validate request data
            $request->validate([
                'stadium_id' => 'required|exists:stadia,id',
                'day' => 'required|in:monday,tuesday,wednesday,thursday,friday,saturday,sunday',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|date_format:H:i',
            ]);

            $workTime = WorkTime::create($request->all());
            return response()->json($workTime, 201);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }
    }

    public function show($id)
    {
        $workTime = WorkTime::findOrFail($id);
        return response()->json($workTime);
    }

    public function update(Request $request, $id)
    {
        // TODO: - ADMIN ROLE OR Stadium Owner

        $workTime = WorkTime::findOrFail($id);
        $workTime->update($request->all());
        return response()->json($workTime, 200);
    }

    public function destroy($id)
    {
        // TODO: - ADMIN ROLE OR Stadium Owner

        $workTime = WorkTime::findOrFail($id);
        $workTime->delete();
        return response()->json(null, 204);
    }
}
