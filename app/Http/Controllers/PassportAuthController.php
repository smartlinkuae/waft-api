<?php

namespace App\Http\Controllers;

use App\Mail\VerificationSuccessMail;
use App\Models\Role;
use App\Models\User;
use App\Models\VerificationCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationCodeMail;
use Illuminate\Validation\Rule;


class PassportAuthController extends Controller
{

    public function sendVerificationCode(User $user)
    {
        // Generate and send the verification code to the user's email
        $verificationCode = rand(100000, 999999);
        // $verificationCode = 111111;
        // Store the verification code in the verification codes table
        VerificationCode::create([
            'user_id' => $user->id,
            'code' => $verificationCode,
        ]);

        Mail::to($user->email)->send(new VerificationCodeMail($verificationCode));
    }

    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => [
                    'required',
                    'email',
                    Rule::unique('users')->where(function ($query) {
                        $query->whereNull('deleted_at');
                        $query->whereNotNull('email_verified_at');
                    })
                ],
                'phone' => [
                    'nullable',
                    'regex:/^(\\+|00)(\\d{4}[0-9]*)$/i',
                    Rule::unique('users')->where(function ($query) {
                        $query->whereNull('deleted_at');
                    }),
                ],
                'password' => 'required|min:3',
                'birthday' => 'nullable|date',
                'gender' => 'nullable|integer|between:1,2',
                'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
                'browsing_mode' => 'required|in:user,stadium_manager',
                'role' => [
                    'nullable',
                    Rule::in(Role::where('name', 'user')->orWhere('name', 'stadium_manager')->pluck('name')->all()),
                ],
            ]);

            // Check if the user's email already exists but is soft-deleted
            $user = User::onlyTrashed()->where('email', $request->email)->first();

            if ($user) {
                // User exists but is soft-deleted, enable the account and continue the registration process

                // Restore the user account
                $user->restore();

                // Update the user's information
                $user->name = $request->name;
                $user->phone = $request->phone;
                $user->birthday = $request->birthday;
                $user->gender = $request->gender;
                $user->password = bcrypt($request->password);
                $user->browsing_mode = $request->browsing_mode;

                // Only update the role if it's a user role or stadium manager
                $roleName = $request->input('role');
                if (in_array($roleName, Role::where('name', 'user')->orWhere('name', 'stadium_manager')->pluck('name')->all())) {
                    $role = Role::where('name', $roleName)->first();
                    $user->role_id = $role->id;
                }

                // Upload the profile image if provided
                if ($request->hasFile('image')) {
                    // Generate a unique filename based on the current date and a random number
                    $filename = date('Y-m-d') . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                    // Move the uploaded image to the storage directory with the new filename
                    $request->file('image')->storeAs('images/profile', $filename, 'public');

                    // Update the user's image with the full path
                    $user->image = url('storage/images/profile/' . $filename);
                }

                // Save the changes
                $user->save();

                // Generate and send the verification code to the user's email
                $this->sendVerificationCode($user);

                return response([
                    'user' => [
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'gender' => $user->gender,
                        'birthday' => $user->birthday,
                        'image' => $user->image,
                        'browsing_mode' => $user->browsing_mode,
                        'role' => $roleName,
                    ],
                    'message' => 'Registration successful. An email has been sent to your email address with a verification code. Please verify your account.',
                ], Response::HTTP_CREATED);
            } else {
                // User does not exist or is not soft-deleted, create a new user account

                $user = new User();

                $user->name = $request->name;
                $user->email = $request->email;
                $user->phone = $request->phone;
                $user->birthday = $request->birthday;
                $user->gender = $request->gender;
                $user->password = bcrypt($request->password);
                $user->browsing_mode = $request->browsing_mode;

                // Only update the role if it's a user role or stadium manager
                $roleName = $request->input('role');
                if (in_array($roleName, Role::where('name', 'user')->orWhere('name', 'stadium_manager')->pluck('name')->all())) {
                    $role = Role::where('name', $roleName)->first();
                    $user->role_id = $role->id;
                }

                // Upload the profile image if provided
                if ($request->hasFile('image')) {
                    // Generate a unique filename based on the current date and a random number
                    $filename = date('Y-m-d') . mt_rand(1000, 9999) . '.' . $request->file('image')->getClientOriginalExtension();

                    // Move the uploaded image to the storage directory with the new filename
                    $request->file('image')->storeAs('images/profile', $filename, 'public');

                    // Update the user's image with the full path
                    $user->image = url('storage/images/profile/' . $filename);
                }

                $user->save();

                // Generate and send the verification code to the user's email
                $this->sendVerificationCode($user);

                // Save the Firebase token to the user's database record
                if ($request->has("firebase_token")) {
                    $user->firebase_token = $request->input("firebase_token");
                    $user->save();
                }


                return response([
                    'user' => [
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'gender' => $user->gender,
                        'birthday' => $user->birthday,
                        'image' => $user->image,
                        'browsing_mode' => $user->browsing_mode,
                        'role' => $roleName,
                    ],
                    'message' => 'Registration successful. An email has been sent to your email address with a verification code. Please verify your account.',
                ], Response::HTTP_CREATED);
            }
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response(['error' => 'An error occurred during registration.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }



    public function verifyCode(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|email|exists:users,email',
                'verification_code' => 'required|digits:6',
            ]);

            $email = $request->input('email'); // I added the email in request to more secure reasons
            $verificationCode = $request->input('verification_code');

            $user = User::where('email', $email)->first();

            $latestCode = VerificationCode::where('user_id', $user->id)
                ->where('code', $verificationCode)
                ->orderBy('created_at', 'desc')
                ->first();

            if (!$latestCode) {
                return response([
                    'error' => 'Invalid verification code.',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user->email_verified_at = now();
            $user->save();

            $latestCode->delete();

            // Send the verification success email
            Mail::to($user->email)->send(new VerificationSuccessMail());

            $token = $user->createToken('auth_Token_Khaled_Stadium')->accessToken;

            // Retrieve the role name using the role_id
            $userRole = Role::find($user->role_id);
            $roleName = $userRole ? $userRole->name : null;
            $user->role =  $roleName;

            // Unset the password field
            unset($user->password);

            return response([
                'user' => $user,
                'token' => $token,
                'message' => 'Verification successful. Your account has been verified.',
            ], Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response(['error' => 'An error occurred during verification.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function login(Request $request)
    {
        try {
			$credentials = $request->validate([
				'email' => 'required|email',
				'password' => 'required|min:3',
			]);

            if (Auth::attempt($credentials)) {
                $user = $request->user();

                // Revoke all old token for avoid open the app on multi device
                ////
                /// = way .1 :
                // DB::table('oauth_access_tokens')
                //     ->where('user_id', Auth::user()->id)
                //     ->update([
                //         'revoked' => true
                //     ]);
                ///
                /// =  way .2 :
                //$user->tokens()->delete();

                // Save the Firebase token to the user's database record
                if ($request->has("firebase_token")) {
                    $user->firebase_token = $request->input("firebase_token");
                    $user->save();
                }

                if (!$user->email_verified_at) {

                    // Generate and send the verification code to the user's email
                    $this->sendVerificationCode($user);

                    return response([
                        'message' => 'Verfication Code sent to your mail',
                        'error' => 'Email not verified.',
                    ], Response::HTTP_UNAUTHORIZED);
                }

                $token = $user->createToken('auth_Token_Khaled_Stadium')->accessToken;

                // Retrieve the role name using the role_id
                $userRole = Role::find($user->role_id);
                $roleName = $userRole ? $userRole->name : null;
                $user->role =  $roleName;

                // Unset the password field
                unset($user->password);

                // Retrieve the role name using the role_id
                $permissions = $user->getPermissions($user->role_id);

                return response([
                    'user' => $user,
                    'permissions' => $permissions,
                    'abilities' => $this->getAbilities($user->role_id),
                    'token' => $token
                ], Response::HTTP_OK);
            }

            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response(['error' => 'An error occurred during login.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function resendVerificationCode(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|email|exists:users,email',
            ]);

            $email = $request->input('email');

            $user = User::where('email', $email)->first();

            // Check if the user's email is already verified
            // if ($user->email_verified_at) {
            //     return response(['message' => 'Email already verified.'], Response::HTTP_OK);
            // }

            // Generate and send the verification code to the user's email
            $this->sendVerificationCode($user);

            return response(['message' => 'Verification code resent successfully.'], Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response(['error' => 'Validation failed.', 'errors' => $e->errors()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response(['error' => 'An error occurred during resending verification code.', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    public function getUser(Request $request)
    {
        return $request->user();
    }

    public function updateToken(Request $request)
    {
        $user = $request->user();
        $token = $user->createToken($request->input('name'));

        return $token->accessToken;
    }

    public function checkToken(Request $request)
    {
        $token_info = $request->user()->token();
        // return $token_info;

        // Token Expired
        if ($token_info->expires_at < now()) {
            $request->user()->token()->revoke();
            $newToken = $request->user()->createToken('auth_Token_Khaled_Labedia')->accessToken;
            return response([
                'token' => $newToken,
                'message' => "Token Refreshed"
            ], Response::HTTP_OK);
        }

        return response([
            'token' => null,
            'message' => "Token is valid"
        ], Response::HTTP_ACCEPTED);
    }

    public function refreshToken(Request $request)
    {
        $user = $request->user();

        // Revoke the current access token
        $user->token()->revoke();

        // Generate a new access token
        $newToken = $user->createToken('auth_Token_Khaled_Stadium')->accessToken;

        return response([
            'token' => $newToken,
            'message' => 'Token refreshed successfully'
        ], Response::HTTP_OK);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['message' => 'Successfully logged out']);
    }

    // If you want to log out from all the devices
    public function logout_all(Request $request)
    {
        DB::table('oauth_access_tokens')
            ->where('user_id', Auth::user()->id)
            ->update([
                'revoked' => true
            ]);


        return response([
            'message' => "logged out from all devices successfully"
        ], Response::HTTP_OK);
    }
    /**
     * Get user details
     *
     * @return \Illuminate\Http\Response
     */
    public function userDetails()
    {
        $user = Auth::user();

        // Retrieve the role name using the role_id
        $userRole = Role::find($user->role_id);
        $roleName = $userRole ? $userRole->name : null;
        $user->role =  $roleName;

        return response()->json(['user' => $user], 200);
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:3|confirmed',
            'token' => 'required',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'), // extracts the required fields from the request payload, including the email, new password, password confirmation, and the reset token.
            function ($user, $password) {
                die('ddd');
                $user->forceFill([
                    'password' => bcrypt($password),
                ])->save();
            }
        );
        var_dump($status);

        if ($status === Password::PASSWORD_RESET) {
            // Password reset successful, delete the password reset token
            DB::table('password_resets')->where('email', $request->email)->delete();

            return response(['message' => 'Password has been reset successfully.'], Response::HTTP_OK);
        } else {
            return response(['error' => 'Password reset failed.'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function forgotPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);
        $status = Password::sendResetLink($request->only('email'));

        if ($status === Password::RESET_LINK_SENT) {
            return response(['message' => 'Password reset link has been sent to your email address.'], Response::HTTP_OK);
        } elseif ($status === Password::INVALID_USER) {
            return response(['error' => 'Email address not found.'], Response::HTTP_NOT_FOUND);
        } elseif ($status === Password::RESET_THROTTLED) {
            return response(['error' => 'Too many password reset requests. Please try again later.'], Response::HTTP_TOO_MANY_REQUESTS);
        } else {
            return response(['error' => 'Unable to send password reset link.'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function getAbilities($role)
    {
        $abilities = [];
        if ($role == 1) {
            // Admin
            $abilities[] = [
                'action' => 'manage',
                'subject' => 'all'
            ];
        } else if ($role == 2) {
            // Manager
            $abilities[] = [
                'action' => 'manage',
                'subject' => 'all'
            ];
        } else if ($role == 4) {
            // User
            $abilities[] = [];
        }
        return $abilities;
    }
}
