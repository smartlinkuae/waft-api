<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FormatDatesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Format request parameters containing date and time
        foreach ($request->all() as $key => $value) {
            if (is_string($value) && $this->isDateFormat($value)) {
                $request->merge([$key => Carbon::parse($value)->format('Y-m-d H:i:s')]);
            }
        }

        // Continue the request handling
        return $next($request);
    }

    /**
     * Check if the given value is in a date format.
     *
     * @param  string  $value
     * @return bool
     */
    private function isDateFormat($value)
    {
        return preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/', $value);
    }
}
