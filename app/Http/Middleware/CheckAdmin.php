<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAdmin
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user() && !$request->user()->isAdmin()) {
            // User is not an admin, handle the unauthorized access
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $next($request);
    }
}
