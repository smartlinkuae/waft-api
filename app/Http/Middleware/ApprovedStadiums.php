<?php

namespace App\Http\Middleware;

use App\Models\Stadium;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApprovedStadiums
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$allowedStatuses)
    {

        // Check if the user is an admin or non-admin
        $isAdmin = Auth::check() && Auth::user()->isAdmin();
        // If the user is not an admin, add a global scope to filter only approved stadiums
        if (!$isAdmin) {
            Stadium::addGlobalScope('status', function ($query) use ($allowedStatuses) {
                $query->whereIn('status', $allowedStatuses);
            });
        }

        return $next($request);

    }
}
