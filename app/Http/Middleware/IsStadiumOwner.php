<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsStadiumOwner
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        $stadiumId = $request->stadium_id;
        
        // Check if the user is the owner of the stadium
        if ($user->ownsStadium($stadiumId)) {
            return $next($request);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }
}
