<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\StadiumActivityController;
use App\Http\Controllers\StadiumController;
use App\Http\Controllers\StadiumImageController;
use App\Http\Controllers\StadiumServiceController;
use App\Http\Controllers\StatisticController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserServiceController;
use App\Http\Controllers\UserStadiumController;
use App\Http\Controllers\WorkTimeController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/welcome', function () {
    return 'Welcome To Stadium From Smart link';
});




Route::post('/register', [PassportAuthController::class, 'register']);
Route::post('/login', [PassportAuthController::class, 'login']);

Route::middleware('auth:api')->group(function () {
    //Auth::routes(['reset' => true]);


    Route::get('/user', [PassportAuthController::class, 'userDetails']);


    Route::post('/token', function (Request $request) {
        $user = $request->user();
        $token = $user->createToken($request->input('name'));

        return $token->accessToken;
    });

    Route::post('/user/refreshToken', [PassportAuthController::class, 'refreshToken']);


    Route::post('/logout', function (Request $request) {
        $request->user()->token()->revoke();
        return response()->json(['message' => 'Successfully logged out']);
    });

    Route::post('logout_all', [PassportAuthController::class, 'logout_all']);
    Route::get('checkToken', [PassportAuthController::class, 'checkToken']);

    Route::post('/updateProfile', [UserController::class, 'updateProfile']);
    Route::post('/sendUpdateEmailOTP', [UserController::class, 'sendUpdateEmailOTP']);
    Route::post('/updateEmail', [UserController::class, 'updateEmail']);

    //Route::post('/password/reset', [PassportAuthController::class, 'resetPassword']);

    Route::delete('/deleteAccount', [UserController::class, 'deleteAccount']);


    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('workTimes', WorkTimeController::class);
    Route::resource('holidays', HolidayController::class);
    Route::resource('reviews', ReviewController::class);
    Route::resource('favorites', FavoriteController::class);
    Route::resource('bookings', BookingController::class);
    Route::resource('userServices', UserServiceController::class);
    Route::resource('packages', PackageController::class);
    Route::resource('stadium-services', StadiumServiceController::class);
    Route::resource('stadium-activities', StadiumActivityController::class);
    Route::resource('user-stadiums', UserStadiumController::class);

    Route::apiResource('stadia.images', StadiumImageController::class)->only(['index', 'store', 'destroy']);

    Route::post('/updateUser/{id}', [UserController::class, 'update']);

    Route::post('banners', [BannerController::class, 'store']);
    Route::post('updateBanner/{id}', [BannerController::class, 'update']);
    Route::delete('banners/{id}', [BannerController::class, 'destroy']);

    Route::post('stadium/{id}/update', [StadiumController::class, 'update']);
    Route::post('activity/{id}/update', [ActivityController::class, 'update']);

    Route::get('/myStadiums', [StadiumController::class, 'myStadiums']);
    Route::get('/myFavoriteStadiums', [StadiumController::class, 'myFavoriteStadiums']);
    Route::get('stadium/{stadiumId}/bookings', [BookingController::class, 'getBookingsForStadium']);
    Route::get('stadium-manager/bookings', [BookingController::class, 'getBookingsForStadiumManager']);
    Route::get('user/bookings', [BookingController::class, 'getUserBookings']);
    Route::get('user/cancelled-bookings', [BookingController::class, 'getCancelledBookings']);
    Route::get('user/completed-bookings', [BookingController::class, 'getUserCompletedBookings']);
    Route::get('user/upcoming-bookings', [BookingController::class, 'getUpcomingBookings']);


    Route::get('/booking/available-times-month', [BookingController::class, 'getAvailableBookingTimesInMonth']);

    //  notification
    Route::post('/notification/send', [NotificationController::class, 'sendNotification']);
    Route::post('/notification/send-with-store', [NotificationController::class, 'sendNotificationWithStore']);
    Route::put('/notification/{id}/mark-as-read', [NotificationController::class, 'markAsRead']);
    Route::get('/notification/unread', [NotificationController::class, 'getUnreadNotifications']);
    Route::get('/notification/all', [NotificationController::class, 'getNotifications']);

    Route::post('/payment/charge', [PaymentController::class, 'createPayment']);
    Route::post('/payment/callback', [PaymentController::class, 'handleCallback'])->name('payment-complete');
    Route::get('/payment/history', [PaymentController::class, 'getPaymentHistory']);
    Route::post('/payment/refund', [PaymentController::class, 'refundPayment'])->name('refund-payment');
    Route::post('/payment/showStatus', [PaymentController::class, 'showStatus']);
    Route::get('/stadium/{stadium}/check-booking', [StadiumController::class, 'hasUserBookedStadium']);

    Route::get('/statistics', [StatisticController::class, 'getStatistics']);
    Route::get('/statistics/stadium-manager', [StatisticController::class, 'stadiumManagerStatistics']);

    Route::post('/stadiums/{id}/change-status', [StadiumController::class, "changeStatus"]);

    Route::post('booking/{id}/cancel', [BookingController::class, 'cancel']);
});

Route::resource('languages', LanguageController::class);

Route::post('/verify-code', [PassportAuthController::class, 'verifyCode']);
Route::post('/password/forgot', [PassportAuthController::class, 'forgotPassword']);
Route::post('/resend-verification-code', [PassportAuthController::class, 'resendVerificationCode']);

// Route::post('/payment/charge/testtt', [PaymentController::class, 'testtt']);
// Route::post('/payment-complete', [PaymentController::class, 'testtt'])->name('payment-complete');

Route::apiResource('settings', SettingController::class);

Route::resource('stadia', StadiumController::class);
Route::get('stadium/searchStadia', [StadiumController::class, 'searchStadia']);
Route::get('stadium/most-booked', [BookingController::class, 'getMostBookedStadiums']);
Route::get('/booking/available-times', [BookingController::class, 'getAvailableBookingTimes']);

Route::resource('activities', ActivityController::class);
Route::resource('services', ServiceController::class);
Route::post('/service/{id}/update', [ServiceController::class, "update"]);
Route::get('banners', [BannerController::class, 'index']);

Route::resource('countries', CountryController::class);
Route::resource('cities', CityController::class);
Route::resource('regions', RegionController::class);
Route::get('stadium/{stadium_id}/reviews', [ReviewController::class, 'stadiumReviews']);

Route::get('getServerTime', [ReviewController::class, 'stadiumReviews']);

Route::get('/getServerTime', function () {
    date_default_timezone_get();
    $timestamp = time(); // Get the current Unix timestamp
    $format = 'Y-m-d H:i:s'; // Specify the desired date and time format

    $date_time = date($format, $timestamp); // Format the timestamp

    // Get the current time zone
    $timezone = date_default_timezone_get();
    echo "Current Time Zone: " . $timezone . PHP_EOL;

    // Get the current GMT (UTC) offset
    $gmt_offset = date('P');
    echo "GMT (UTC) Offset: " . $gmt_offset . PHP_EOL;


    return "<h1> $date_time</h1>";
});
