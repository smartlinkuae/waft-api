<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear existing data from the table
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        // Seed new records
        User::create([
            'name' => 'khaled El Debuch',
            'email' => 'eng.khaled.de@gmail.com',
            'password' => Hash::make('Khaled112233'),
            'phone' => '+963934692200',
            'role_id' => 1, // Replace with the appropriate role_id
        ]);
    }
}
