<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear existing data from the table
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('languages')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('languages')->insert([
            
            [
                'name' => 'English',
                'code' => 'en',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'عربي',
                'code' => 'ar',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'French',
                'code' => 'fr',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'German',
                'code' => 'de',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            // Add more languages as needed
        ]);
    }
}
