<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Stadium;
use Illuminate\Database\Seeder;

class StadiumActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get all stadiums and activities
        $stadiums = Stadium::all();
        $activities = Activity::all();

        // Seed the pivot table
        foreach ($stadiums as $stadium) {
            // Randomly attach activities to stadiums
            $stadium->activities()->attach($activities->random(rand(1, 3))->pluck('id'));
        }
    }
}
