<?php

namespace Database\Seeders;

use App\Models\Stadium;
use Illuminate\Database\Seeder;

class StadiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Define stadiums for UAE
        $stadiums = [
            [
                'name_en' => 'Mohammed bin Zayed Stadium',
                'name_ar' => 'ملعب محمد بن زايد',
                'activity_id' => 1,
            ],
            [
                'name_en' => 'Hazza Bin Zayed Stadium',
                'name_ar' => '',
                'activity_id' => 1,
            ],
            [
                'name_en' => 'Zabeel Stadium',
                'name_ar' => '',
                'activity_id' => 1,
            ],
            [
                'name_en' => 'Rashid Stadium',
                'name_ar' => 'ملعب راشد',
                'activity_id' => 1,
            ],
            [
                'name_en' => 'Al-Maktoum Stadium',
                'name_ar' => 'ملعب المكتوم',
                'activity_id' => 1,
            ],
            [
                'name_en' => 'Al Nahyan Stadium',
                'name_ar' => '',
                'activity_id' => 1,
            ],
            // Add more stadiums for UAE here
        ];

        // Insert stadiums for UAE
        Stadium::insert($stadiums);
    }
}
