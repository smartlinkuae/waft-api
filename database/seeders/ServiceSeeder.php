<?php

namespace Database\Seeders;

use App\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create services
        Service::create([
            'name_en' => 'Parking',
            'name_ar' => 'الوقوف',
            'icon' => 'parking.png',
        ]);

        Service::create([
            'name_en' => 'Restaurant',
            'name_ar' => 'المطعم',
            'icon' => 'restaurant.png',
        ]);

        Service::create([
            'name_en' => 'WiFi',
            'name_ar' => 'الوايفاي',
            'icon' => 'wifi.png',
        ]);

        Service::create([
            'name_en' => 'Gym',
            'name_ar' => 'Gym',
            'icon' => 'gym.png',
        ]);
    }
}
