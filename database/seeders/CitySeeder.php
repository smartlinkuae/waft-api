<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable foreign key checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        // Clear existing data in the cities table
        City::truncate();

        // Insert cities


        // Get the UAE country ID
        $uae = Country::where('iso_code', 'AE')->first();

        // List of cities in the UAE
        $cities = [
            ['en' => 'Dubai', 'ar' => 'دبي'],
            ['en' => 'Abu Dhabi', 'ar' => 'أبو ظبي'],
            ['en' => 'Sharjah', 'ar' => 'الشارقة'],
            ['en' => 'Al Ain', 'ar' => 'العين'],
            ['en' => 'Western Region', 'ar' => 'المناطق الغربية'],
            ['en' => 'Abu Dhabi Al Ain Road', 'ar' => 'طريق ابوظبي العين'],
            ['en' => 'Hatta', 'ar' => 'حتا'],
            ['en' => 'Fujairah', 'ar' => 'الفجيرة'],
            ['en' => 'Ajman', 'ar' => 'عجمان'],
            ['en' => 'Ras Al Khaimah', 'ar' => 'رأس الخيمة'],
            ['en' => 'Umm Al Quwain', 'ar' => 'أم القيوين'],
            // Add more cities with their English and Arabic names as needed
        ];

        // Insert cities for United Arab Emirates
        foreach ($cities as $city) {
            City::create([
                'name_en' => $city['en'],
                'name_ar' => $city['ar'],
                'country_id' => $uae->id,
            ]);
        }

        // Enable foreign key checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
