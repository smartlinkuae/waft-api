<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'admin')->first();
        $userRole = Role::where('name', 'user')->first();

        // Assign permissions to roles
        $adminPermissions = ['create_users', 'read_users', 'update_users', 'delete_users'];
        $userPermissions = ['create_posts', 'read_posts', 'update_posts', 'delete_posts'];

        foreach ($adminPermissions as $permissionName) {
            $permission = Permission::where('name', $permissionName)->first();
            $adminRole->permissions()->attach($permission);
        }

        foreach ($userPermissions as $permissionName) {
            $permission = Permission::where('name', $permissionName)->first();
            $userRole->permissions()->attach($permission);
        }
    }
}
