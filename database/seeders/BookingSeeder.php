<?php

namespace Database\Seeders;

use App\Models\Booking;
use App\Models\Stadium;
use App\Models\User;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get all stadiums and users
        $stadiums = Stadium::all();
        $users = User::all();

        // Seed bookings
        foreach ($stadiums as $stadium) {
            // Create a booking for each stadium and user combination
            foreach ($users as $user) {
                Booking::create([
                    'stadium_id' => $stadium->id,
                    'user_id' => $user->id,
                    'start_time' => now(),
                    'end_time' => now()->addHours(2),
                ]);
            }
        }
    }
}
