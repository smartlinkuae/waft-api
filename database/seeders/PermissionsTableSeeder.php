<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear existing data from the table
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $permissions = [
            ['name' => 'create_users'],
            ['name' => 'read_users'],
            ['name' => 'update_users'],
            ['name' => 'delete_users'],
            ['name' => 'create_posts'],
            ['name' => 'read_posts'],
            ['name' => 'update_posts'],
            ['name' => 'delete_posts'],
            // Add more permissions if needed
        ];

        Permission::insert($permissions);
    }
}
