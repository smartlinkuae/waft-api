<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);

        $this->call(LanguageSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CitySeeder::class);
        $this->call(RegionSeeder::class);

        $this->call(ActivitySeeder::class);
        $this->call(StadiaSeeder::class);

        // --$this->call(BookingSeeder::class);
        $this->call(MembershipSeeder::class);
        // --$this->call(MembershipUserSeeder::class);
        // --$this->call(StadiumActivitySeeder::class);

        $this->call(ServiceSeeder::class);
    }
}
