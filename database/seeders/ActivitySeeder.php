<?php

namespace Database\Seeders;

use App\Models\Activity;
use Illuminate\Database\Seeder;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activities = [
            ['name_en' => 'Football', 'name_ar' => 'كرة القدم'],
            ['name_en' => 'Basketball', 'name_ar' => 'كرة السلة'],
            ['name_en' => 'Tennis', 'name_ar' => 'كرة التنس'],
            // Add more activities as needed
        ];

        foreach ($activities as $activity) {
            Activity::create($activity);
        }
    }
}
