<?php

namespace Database\Seeders;

use App\Models\Membership;
use App\Models\User;
use Illuminate\Database\Seeder;

class MembershipUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get all memberships and users
        $memberships = Membership::all();
        $users = User::all();

        // Seed membership_user pivot table
        foreach ($memberships as $membership) {
            // Attach random users to each membership
            $membership->users()->attach($users->random(rand(1, 3))->pluck('id'));
        }
    }
}
