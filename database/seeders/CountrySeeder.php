<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Clear existing data from the table
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('countries')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // Insert countries
        Country::create([
            'name_en' => 'United Arab Emirates',
            'name_ar' => 'الإمارت',
            'iso_code' => 'AE',
        ]);

        Country::create([
            'name_en' => 'Kuwait',
            'name_ar' => 'الكويت',
            'iso_code' => 'KW',
        ]);

        Country::create([
            'name_en' => 'Saudi Arabia',
            'name_ar' => 'السعودية',
            'iso_code' => 'SA',
        ]);

        Country::create([
            'name_en' => 'Qatar',
            'name_ar' => 'قطر',
            'iso_code' => 'QA',
        ]);

        Country::create([
            'name_en' => 'Oman',
            'name_ar' => 'عمان',
            'iso_code' => 'OM',
        ]);


    }
}
