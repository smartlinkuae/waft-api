<?php

namespace Database\Seeders;

use App\Models\Membership;
use Illuminate\Database\Seeder;

class MembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Define membership data
        $memberships = [
            ['name_en' => 'Gold Membership', 'name_ar' => 'العضوية الذهبية', 'duration' => 'annual'],
            ['name_en' => 'Silver Membership', 'name_ar' => '', 'duration' => 'monthly'],
            ['name_en' => 'Bronze Membership', 'name_ar' => '', 'duration' => 'monthly'],
            ['name_en' => 'Platinum Membership', 'name_ar' => '', 'duration' => 'annual'],
            ['name_en' => 'Basic Membership', 'name_ar' => '', 'duration' => 'daily'],
        ];

        // Seed memberships
        foreach ($memberships as $membership) {
            Membership::create($membership);
        }
    }
}
