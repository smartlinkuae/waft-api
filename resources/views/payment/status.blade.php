<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Payment Status</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f4f4f4;
                margin: 0;
                padding: 20px;
            }

            .container {
                max-width: 600px;
                margin: 0 auto;
                background-color: #fff;
                border-radius: 5px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                padding: 20px;
            }

            h1 {
                margin-top: 0;
                color: #333;
            }

            p {
                margin-bottom: 10px;
            }

            .status {
                font-weight: bold;
            }

            .status.success {
                color: #28a745;
            }

            .status.failed {
                color: #dc3545;
            }

            .button-container {
                margin-top: 20px;
                text-align: center
            }

            .button {
                padding: 10px 20px;
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                text-decoration: none;
                font-weight: bold;
                cursor: pointer;
            }

            .button:hover {
                background-color: #0056b3;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <h1>Payment Status</h1>
            <p><strong>Transaction ID:</strong> {{ $payment->transaction_id }}</p>
            <p><strong>Status:</strong> <span class="status {{ $payment->status === 'CAPTURED' ? 'success' : 'failed' }}">{{ $payment->status }}</span></p>
            <p><strong>Amount:</strong> {{ $payment->amount }}  {{$payment->currency}}</p>
            <!-- Add more details as needed -->

            <div class="button-container">
                <a href="https://waft.ae/bookingResult?bookingId={{ $payment->booking_id }}&result={{ $payment->status === 'CAPTURED' ? 'success' : 'failed' }}" class="button">Open in App</a>
            </div>
        </div>
    </body>

</html>
