<!DOCTYPE html>
<html>
<head>
    <title>Verification Code From Waft App</title>
</head>
<body>
    <h1>Verification Code</h1>
    <h2>Waft App</h2>
    <p>  Here is your verification code: <strong>{{ $verificationCode }}</strong></p>
</body>
</html>
